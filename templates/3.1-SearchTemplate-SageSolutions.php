<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
			
			<?php include('inc/i-hero-inside.php'); ?>
			
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite home-sm">Sage Solutions Home</a>
					<a href="#">Search Results</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<div class="body">
					
				<div class="header">
					<div class="sw">
						<h1>Search Results (h1)</h1>
						<span class="subtitle">Lorem Ipsum Dolar Sit Amet</span>
					</div><!-- .sw -->
				</div><!-- .header -->
				
				<!-- news results blocks -->
				<div class="on-white blocks">
					<div class="sw">
					
						<div class="action-bar">
						
							<div class="controls">
								<button class="control sprite-after abs arr-sm-left-white prev">Prev</button>
								<button class="control sprite-after abs arr-sm-right-white next">Next</button>
							</div><!-- .controls -->
						
							<div class="count">
								<span class="num">10</span> News Results
							</div><!-- .count -->
							
						</div><!-- .action-bar -->
					
						<div class="swiper-wrapper collapse-750">
							<div class="swipe">
								<div class="swipe-wrap">
									<div>
										
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-2.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-3.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
											</div><!-- .grid -->
										</div><!-- .grid-wrap -->
										
									</div>

									<div>
									
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-2.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-3.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
											</div><!-- .grid -->
										</div><!-- .grid-wrap -->
										
									</div>
								
									<div>
										
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-2.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

											</div><!-- .grid -->
										</div><!-- .grid-wrap -->										
									</div>

								
								</div><!-- .swipe-wrap -->
							</div><!-- .swipe -->
						</div><!-- .swiper-wrapper -->
						
					</div><!-- .sw -->
				</div><!-- ..blocks.on-white -->
				
				<!-- event results blocks -->
				<div class="on-white blocks">
					<div class="sw">
					
						<div class="action-bar">
						
							<div class="controls">
								<button class="control sprite-after abs arr-sm-left-white prev">Prev</button>
								<button class="control sprite-after abs arr-sm-right-white next">Next</button>
							</div><!-- .controls -->
						
							<div class="count">
								<span class="num">8</span> Event Results
							</div><!-- .count -->
							
						</div><!-- .action-bar -->
					
						<div class="swiper-wrapper collapse-750">
							<div class="swipe">
								<div class="swipe-wrap">
									<div>
										
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-2.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-3.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
											</div><!-- .grid -->
										</div><!-- .grid-wrap -->
										
									</div>

									<div>
									
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-2.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-3.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
											</div><!-- .grid -->
										</div><!-- .grid-wrap -->
										
									</div>
								
									<div>
										
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-2.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

											</div><!-- .grid -->
										</div><!-- .grid-wrap -->										
									</div>

								
								</div><!-- .swipe-wrap -->
							</div><!-- .swipe -->
						</div><!-- .swiper-wrapper -->
						
					</div><!-- .sw -->
				</div><!-- ..blocks.on-white -->

				<!-- page results blocks -->
				<div class="on-white blocks">
					<div class="sw">
					
						<div class="action-bar">
						
							<div class="controls">
								<button class="control sprite-after abs arr-sm-left-white prev">Prev</button>
								<button class="control sprite-after abs arr-sm-right-white next">Next</button>
							</div><!-- .controls -->
						
							<div class="count">
								<span class="num">2</span> Page Results
							</div><!-- .count -->
							
						</div><!-- .action-bar -->
					
						<div class="swiper-wrapper collapse-750">
							<div class="swipe">
								<div class="swipe-wrap">
									<div>
										
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
											</div><!-- .grid -->
										</div><!-- .grid-wrap -->
										
									</div>
								
								</div><!-- .swipe-wrap -->
							</div><!-- .swipe -->
						</div><!-- .swiper-wrapper -->
						
					</div><!-- .sw -->
				</div><!-- ..blocks.on-white -->
				
				<!-- services results blocks -->
				<div class="on-white blocks">
					<div class="sw">
					
						<div class="action-bar">
						
							<div class="controls">
								<button class="control sprite-after abs arr-sm-left-white prev">Prev</button>
								<button class="control sprite-after abs arr-sm-right-white next">Next</button>
							</div><!-- .controls -->
						
							<div class="count">
								<span class="num">3</span> Services Results
							</div><!-- .count -->
							
						</div><!-- .action-bar -->
					
						<div class="swiper-wrapper collapse-750">
							<div class="swipe">
								<div class="swipe-wrap">
									<div>
										
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
												
												<div class="col-3 block-col col">
													<div class="item">
													
														<a class="block with-img" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
											</div><!-- .grid -->
										</div><!-- .grid-wrap -->
										
									</div>
								
								</div><!-- .swipe-wrap -->
							</div><!-- .swipe -->
						</div><!-- .swiper-wrapper -->
						
					</div><!-- .sw -->
				</div><!-- ..blocks.on-white -->
				
			</div><!-- .body -->
			
<?php include('inc/i-footer.php'); ?>