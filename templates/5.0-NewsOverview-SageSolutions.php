<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
			
			<?php include('inc/i-hero-inside.php'); ?>
			
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite home-sm">Sage Solutions Home</a>
					<a href="#">The Latest</a>
					<a href="#">News</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<div class="body">
					
					<div class="header">
						<div class="sw">
							<h1>News (h1)</h1>
							<span class="subtitle">Lorem Ipsum Dolar Sit Amet</span>
						</div><!-- .sw -->
					</div><!-- .header -->
					
				<div class="sw cf">
					<div class="main-body">
						<div class="article-body">
							
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo 
							commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla 
							luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
							
						</div><!-- .article-body -->
					</div><!-- .main-body -->
				</div><!-- .sw -->
				
				<!-- overview blocks -->
				<div class="on-white blocks">
					<div class="sw">
					
						<div class="action-bar">
						
							<div class="controls">
								<button class="control sprite-after abs arr-sm-left-white prev">Prev</button>
								<button class="control sprite-after abs arr-sm-right-white next">Next</button>
							</div><!-- .controls -->
						
							<form action="/" method="get" class="single-form search-form">
								<fieldset>
									<input type="text" name="s" placeholder="Search news...">
									<input type="hidden" name="post_type" value="news">
									<button type="submit" class="sprite-after abs search-hover" title="Search News">Search News</button>
								</fieldset>
							</form><!-- .single-form.search-form -->
						
							<div class="count">
								<span class="num">10</span> articles found
							</div><!-- .count -->
							
						</div><!-- .action-bar -->
					
						<div class="swiper-wrapper">
							<div class="swipe">
								<div class="swipe-wrap">
									<div>
										
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
											
												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<span class="subtitle">Lorem Ipsum Dolor sit Amet</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<span class="subtitle">Lorem Ipsum Dolor sit Amet</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<span class="subtitle">Lorem Ipsum Dolor sit Amet</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
											</div><!-- .grid -->
										</div><!-- .grid-wrap -->
										
									</div>

									<div>
										
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
											
												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<span class="subtitle">Lorem Ipsum Dolor sit Amet</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<span class="subtitle">Lorem Ipsum Dolor sit Amet</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<span class="subtitle">Lorem Ipsum Dolor sit Amet</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->
											</div><!-- .grid -->
										</div><!-- .grid-wrap -->
										
									</div>
								
									<div>
										
										<div class="grid-wrap">
											<div class="grid eqh collapse-no-flex">
											
												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<span class="subtitle">Lorem Ipsum Dolor sit Amet</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

												<div class="col-3 block-col">
													<div class="item">
													
														<a class="block with-img with-meta" href="#">
															<div class="img-wrap">
																<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
															</div><!-- .img-wrap -->
															<div class="content">
																<span class="title">Vivamus Consectetur Pellentesque</span>
																<span class="subtitle">Lorem Ipsum Dolor sit Amet</span>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																<span class="button green">Read More</span>
															</div><!-- .content -->
															<div class="meta">
																<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
																<span class="button green category">Category</span>
															</div><!-- .meta -->
														</a><!-- .block -->
														
													</div><!-- .item -->
												</div><!-- .col-3 -->

											</div><!-- .grid -->
										</div><!-- .grid-wrap -->
										
									</div>

								
								</div><!-- .swipe-wrap -->
							</div><!-- .swipe -->
						</div><!-- .swiper-wrapper -->
						
					</div><!-- .sw -->
				</div><!-- ..blocks.on-white -->
				
			</div><!-- .body -->
			
			<?php include('inc/i-how-can-we-help.php'); ?>
			
<?php include('inc/i-footer.php'); ?>