<div class="grid-wrap">
	<div class="grid fill eqh collapse-no-flex centered">
	
		<div class="col-3 block-col col">
			<div class="item">
			
				<a class="block with-img" href="#">
					<div class="img-wrap">
						<div class="img" style="background-image: url(../assets/images/temp/services/service-1.jpg);"></div>
					</div><!-- .img-wrap -->
					<div class="content">
						<span class="title">Vivamus Consectetur Pellentesque</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
						<span class="button green">Read More</span>
					</div><!-- .content -->
				</a><!-- .block -->
				
			</div><!-- .item -->
		</div><!-- .col-3 -->

		<div class="col-3 block-col col">
			<div class="item">
			
				<a class="block with-img" href="#">
					<div class="img-wrap">
						<div class="img" style="background-image: url(../assets/images/temp/services/service-2.jpg);"></div>
					</div><!-- .img-wrap -->
					<div class="content">
						<span class="title">Vivamus Consectetur</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
						<span class="button green">Read More</span>
					</div><!-- .content -->
				</a><!-- .block -->
				
			</div><!-- .item -->
		</div><!-- .col-3 -->

		<div class="col-3 block-col col">
			<div class="item">
			
				<a class="block with-img" href="#">
					<div class="img-wrap">
						<div class="img" style="background-image: url(../assets/images/temp/services/service-3.jpg);"></div>
					</div><!-- .img-wrap -->
					<div class="content">
						<span class="title">Vivamus Consectetur</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
						<span class="button green">Read More</span>
					</div><!-- .content -->
				</a><!-- .block -->
				
			</div><!-- .item -->
		</div><!-- .col-3 -->

		<div class="col-3 block-col col">
			<div class="item">
			
				<a class="block with-img" href="#">
					<div class="img-wrap">
						<div class="img" style="background-image: url(../assets/images/temp/services/service-4.jpg);"></div>
					</div><!-- .img-wrap -->
					<div class="content">
						<span class="title">Vivamus Consectetur</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
						<span class="button green">Read More</span>
					</div><!-- .content -->
				</a><!-- .block -->
				
			</div><!-- .item -->
		</div><!-- .col-3 -->

		<div class="col-3 block-col col">
			<div class="item">
			
				<a class="block with-img" href="#">
					<div class="img-wrap">
						<div class="img" style="background-image: url(../assets/images/temp/services/service-1.jpg);"></div>
					</div><!-- .img-wrap -->
					<div class="content">
						<span class="title">Vivamus Consectetur Pellentesque</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
						<span class="button green">Read More</span>
					</div><!-- .content -->
				</a><!-- .block -->
				
			</div><!-- .item -->
		</div><!-- .col-3 -->

		<div class="col-3 block-col col">
			<div class="item">
			
				<a class="block with-img" href="#">
					<div class="img-wrap">
						<div class="img" style="background-image: url(../assets/images/temp/services/service-2.jpg);"></div>
					</div><!-- .img-wrap -->
					<div class="content">
						<span class="title">Vivamus Consectetur</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
						<span class="button green">Read More</span>
					</div><!-- .content -->
				</a><!-- .block -->
				
			</div><!-- .item -->
		</div><!-- .col-3 -->

		<div class="col-3 block-col col">
			<div class="item">
			
				<a class="block with-img" href="#">
					<div class="img-wrap">
						<div class="img" style="background-image: url(../assets/images/temp/services/service-3.jpg);"></div>
					</div><!-- .img-wrap -->
					<div class="content">
						<span class="title">Vivamus Consectetur</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
						<span class="button green">Read More</span>
					</div><!-- .content -->
				</a><!-- .block -->
				
			</div><!-- .item -->
		</div><!-- .col-3 -->

		<div class="col-3 block-col col">
			<div class="item">
			
				<a class="block with-img" href="#">
					<div class="img-wrap">
						<div class="img" style="background-image: url(../assets/images/temp/services/service-4.jpg);"></div>
					</div><!-- .img-wrap -->
					<div class="content">
						<span class="title">Vivamus Consectetur</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
						<span class="button green">Read More</span>
					</div><!-- .content -->
				</a><!-- .block -->
				
			</div><!-- .item -->
		</div><!-- .col-3 -->

		<div class="col-3 block-col col">
			<div class="item">
			
				<a class="block with-img" href="#">
					<div class="img-wrap">
						<div class="img" style="background-image: url(../assets/images/temp/services/service-3.jpg);"></div>
					</div><!-- .img-wrap -->
					<div class="content">
						<span class="title">Vivamus Consectetur</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
						<span class="button green">Read More</span>
					</div><!-- .content -->
				</a><!-- .block -->
				
			</div><!-- .item -->
		</div><!-- .col-3 -->

		<div class="col-3 block-col col">
			<div class="item">
			
				<a class="block with-img" href="#">
					<div class="img-wrap">
						<div class="img" style="background-image: url(../assets/images/temp/services/service-4.jpg);"></div>
					</div><!-- .img-wrap -->
					<div class="content">
						<span class="title">Vivamus Consectetur</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
						<span class="button green">Read More</span>
					</div><!-- .content -->
				</a><!-- .block -->
				
			</div><!-- .item -->
		</div><!-- .col-3 -->
		
	</div><!-- .grid.eqh -->
</div><!-- .grid-wrap -->