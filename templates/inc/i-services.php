<div class="blocks">
	<div class="sw">
		<div class="action-bar">
		
			<div class="controls">
				<button class="control sprite-after abs arr-sm-left-white prev">Prev</button>
				<button class="control sprite-after abs arr-sm-right-white next">Next</button>
			</div><!-- .controls -->
		
			<div class="count">
				<span class="num">10</span> services found
			</div><!-- .count -->
		</div><!-- .action-bar -->
		
		<div class="services-container">
		
			<!-- 
				NOTES:
				
				HTML for all services will be inside this div - easy access to filter quickly with JS
				If a service belongs to more than one category, just add it data-services like data-services='["yourself","relationships"]'
				
			-->
		
			<div class="services-source i-hidden">

				<div class="col-3 block-col col service" data-services='["company"]'>
					<div class="item">
					
						<a class="block with-img" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/services/service-1.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<span class="title">Vivamus Consectetur Pellentesque</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .service -->

				<div class="col-3 block-col col service" data-services='["physical-health"]'>
					<div class="item">
					
						<a class="block with-img" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/services/service-2.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<span class="title">Vivamus Consectetur</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col-3 -->

				<div class="col-3 block-col col service" data-services='["child-teen"]'>
					<div class="item">
					
						<a class="block with-img" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/services/service-3.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<span class="title">Vivamus Consectetur</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col-3 -->

				<div class="col-3 block-col col service" data-services='["relationships"]'>
					<div class="item">
					
						<a class="block with-img" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/services/service-4.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<span class="title">Vivamus Consectetur</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col-3 -->
				
				<div class="col-3 block-col col service" data-services='["yourself"]'>
					<div class="item">
					
						<a class="block with-img" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/services/service-1.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<span class="title">Vivamus Consectetur Pellentesque</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col-3 -->
			
				<div class="col-3 block-col col service" data-services='["yourself"]'>
					<div class="item">
					
						<a class="block with-img" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/services/service-1.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<span class="title">Vivamus Consectetur Pellentesque</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .service -->

				<div class="col-3 block-col col service" data-services='["relationships"]'>
					<div class="item">
					
						<a class="block with-img" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/services/service-2.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<span class="title">Vivamus Consectetur</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col-3 -->

				<div class="col-3 block-col col service" data-services='["child-teen"]'>
					<div class="item">
					
						<a class="block with-img" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/services/service-3.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<span class="title">Vivamus Consectetur</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col-3 -->

				<div class="col-3 block-col col service" data-services='["physical-health"]'>
					<div class="item">
					
						<a class="block with-img" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/services/service-4.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<span class="title">Vivamus Consectetur</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col-3 -->
				
				<div class="col-3 block-col col service" data-services='["company"]'>
					<div class="item">
					
						<a class="block with-img" href="#">
							<div class="img-wrap">
								<div class="img" style="background-image: url(../assets/images/temp/services/service-1.jpg);"></div>
							</div><!-- .img-wrap -->
							<div class="content">
								<span class="title">Vivamus Consectetur Pellentesque</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col-3 -->

			</div><!-- .services.i-hidden -->
		
			<div class="services-swiper">
				
			</div><!-- .services-swiper -->
		
		</div><!-- .services-container -->
		
	</div><!-- .sw -->
</div><!-- .blocks -->