	<div class="blocks always-list list-view">
		<div class="action-bar">
			<div class="count">
				<span class="num">4</span> answers found
			</div><!-- .count -->
		</div><!-- .action-bar -->
	
		<div class="grid-wrap">
			<div class="grid eqh collapse-no-flex">
			
				<div class="col-3 block-col col">
					<div class="item">
					
						<a class="block" href="#">
							<div class="content">
								<span class="title">Vivamus Consectetur Pellentesque</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col-3 -->
				
				<div class="col-3 block-col col">
					<div class="item">
					
						<a class="block" href="#">
							<div class="content">
								<span class="title">Vivamus Consectetur Pellentesque</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col-3 -->

				<div class="col-3 block-col col">
					<div class="item">
					
						<a class="block" href="#">
							<div class="content">
								<span class="title">Vivamus Consectetur Pellentesque</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col-3 -->

				<div class="col-3 block-col col">
					<div class="item">
					
						<a class="block" href="#">
							<div class="content">
								<span class="title">Vivamus Consectetur Pellentesque</span>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
								<span class="button green">Read More</span>
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col-3 -->
				
			</div><!-- .grid -->
		</div><!-- .grid-wrap -->
	</div><!-- .blocks -->