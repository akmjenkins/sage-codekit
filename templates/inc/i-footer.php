					
			<footer>
			
				<div class="sw">
					<div class="grid-wrap">
						<div class="grid eqh fill pad5">
							
								<div class="col-3 col">
									<div class="item footer-contact">
										<a href="#" class="footer-logo">Sage Solutions</a>
										
										<div class="address-block">
											<address>
												70 Rue King Street <br />
												Moncton NB E1C 4M6
											</address>
											
											<div class="phones">
												<span> P 1 506 857 3258</span>
												<span>TF 1 800 390 3258</span>
											</div><!-- .phones -->
										</div><!-- .address-block -->
										
									</div><!-- .item -->
								</div><!-- .col-3 -->
								
								<div class="col-3 col">
									<div class="item footer-nav">
										
										<ul>
											<li><a href="#">How We Can Help?</a></li>
											<li><a href="#">Our Services</a></li>
											<li><a href="#">Corporate Services</a></li>
											<li><a href="#">Service Providers</a></li>
											<li><a href="#">Who We Are</a></li>
										</ul>
										
									</div><!-- .item -->
								</div><!-- .col-3 -->
								
								<div class="col-3 col">
									<div class="item footer-book-appointment">
										
										<p>Need help? We would love to talk with you. Book an appointment today.</p>
										
										<a href="#" class="green button">Book An Appointment</a>
										
									</div><!-- .item -->
								</div><!-- .col-3 -->
								
						</div><!-- .grid -->
					</div><!-- .grid-wrap -->
				</div><!-- .sw -->
				
				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Sage Solutions Inc.</a></li>
							<li><a href="#">Become a Service Provider</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac" class="replace">JAC. We Create.</a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
			
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/sage-codekit',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script src="../assets/js/min/main-min.js"></script>
	</body>
</html>