<div class="nav">
	<div class="nav-inner-wrap">	
		<nav>
			<div class="sw">
				<ul>
					<li>
						<a href="#" class="sprite-after home">Sage Solutions</a>
					</li>
					<li>
						<a href="#">How We Can Help?</a>
						<div>
							<ul>
								<li><a href="#">Take Care of Yourself</a></li>
								<li><a href="#">Your Relationships</a></li>
								<li><a href="#">Your Children</a></li>
								<li><a href="#">Physical Health</a></li>
								<li><a href="#">Company</a></li>
							</ul>
							
							<div class="swiper-wrapper">
								<div class="swipe-to-be">
									<div class="swipe-wrap">
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Take Care of Yourself</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta" >
												<span class="dd-title">Your Relationships</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta">
												<span class="dd-title">Your Children</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta">
												<span class="dd-title">Physical Health</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>	
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta">
												<span class="dd-title">Company</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
									</div><!-- .swipe-wrap -->
								</div><!-- .swipe -->
							</div><!-- .swiper-wrapper -->
							
						</div>
					</li>
					<li>
						<a href="#">Our Services</a>
						<div>
							<ul>
								<li><a href="#">Personal Counselling</a></li>
								<li><a href="#">Psychology Services &amp; Assessment</a></li>
								<li><a href="#">Family Mediation</a></li>
								<li><a href="#">Alternative Health Services</a></li>
								<li><a href="#">Group Programs</a></li>
							</ul>
							
							<div class="swiper-wrapper">
								<div class="swipe-to-be">
									<div class="swipe-wrap">
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Personal Counselling</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta" >
												<span class="dd-title">Psychology Services &amp; Assessment</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta">
												<span class="dd-title">Family Mediation</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta">
												<span class="dd-title">Alternative Health Services</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>	
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta">
												<span class="dd-title">Group Programs</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
									</div><!-- .swipe-wrap -->
								</div><!-- .swipe -->
							</div><!-- .swiper-wrapper -->
							
						</div>
					</li>
					<li>
						<a href="#">Corporate Services</a>
						<div>
							<ul>
								<li><a href="#">Employee Assistance Program</a></li>
								<li><a href="#">Conflict Resolution</a></li>
								<li><a href="#">Critical Incident Stress Management</a></li>
								<li><a href="#">Workshop Educational Sessions</a></li>
								<li><a href="#">Corporate Wellness</a></li>
							</ul>
							
							<div class="swiper-wrapper">
								<div class="swipe-to-be">
									<div class="swipe-wrap">
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Employee Assistance Program</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta" >
												<span class="dd-title">Conflict Resolution</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta">
												<span class="dd-title">Critical Incident Stress Management</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta">
												<span class="dd-title">Workshop Educational Sessions</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>	
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta">
												<span class="dd-title">Corporate Wellness</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
									</div><!-- .swipe-wrap -->
								</div><!-- .swipe -->
							</div><!-- .swiper-wrapper -->

						</div>
					</li>
					<li>
						<a href="#">Service Providers</a>
					</li>
					<li>
						<a href="#">Who We Are</a>
						<div>
							<ul>
								<li><a href="#">Our Apporach</a></li>
								<li><a href="#">Our Service Professionals</a></li>
								<li><a href="#">Our Location</a></li>
								<li><a href="#">Become a Service Provider</a></li>
							</ul>
							
							<div class="swiper-wrapper">
								<div class="swipe-to-be">
									<div class="swipe-wrap">
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Our Approach</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta" >
												<span class="dd-title">Our Service Professionals</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta">
												<span class="dd-title">Our Location</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>
											</div><!-- .meta -->
											
										</div>
										<div>
										
											<div class="img" data-img="assets/images/temp/dropdown.jpg"></div>
											<div class="meta">
												<span class="dd-title">Become a Service Provider</span>
												<p>Aenean sed mi porttitor, pretium massa a, cursus sem. Integer elit urna, aliquam tristique auctor id, gravida non ipsum.</p>	
											</div><!-- .meta -->
											
										</div>
									</div><!-- .swipe-wrap -->
								</div><!-- .swipe -->
							</div><!-- .swiper-wrapper -->
							
						</div>
					</li>
				</ul>
				<div class="nav-book-appointment">
					<button class="button green with-ico sprite-after circle-arrow-down">Book An Appointment</button>
					<form class="body-form" method="post">
						<fieldset>
							<input type="text" name="name" placeholder="Name">
							<input type="email" name="email" placeholder="Email Address">
							<textarea name="message" cols="30" rows="10" placeholder="Message"></textarea>
							<button type="submit" class="button white">Submit</button>
						</fieldset>
						
						<div>
							<span>or call</span>
							<span>1.800.390.3258</span>
						</div>
						
					</form><!-- .body-form -->
				</div>
			</div><!-- .sw -->
		</nav>
	
		<div class="top">
			<div class="sw">
				<div class="lang">
					<a href="#" class="selected">En</a>
					<a href="#">Fr</a>
				</div><!-- .lang -->
				
				<div class="meta-nav">
					<a href="#">Log In</a>
					<a href="#">Resources</a>
					<a href="#">The Latest</a>
					<a href="#">Contact Us</a>
				</div><!-- .meta-nav -->
				
				<?php include('i-social.php'); ?>
				
				<div class="search-area">
					<button class="sprite search" title="Search"></button>
					<?php include('i-search-form.php'); ?>
				</div><!-- .search-area -->
				
			</div><!-- .sw -->
		</div><!-- .top -->

	</div><!-- .nav-inner-wrap -->
</div><!-- .nav -->