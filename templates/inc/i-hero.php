<div class="hero">
	<div class="swiper-wrapper button-controls">
		<div class="swipe" data-controls="true" data-links="true" data-auto="7">
			<div class="swipe-wrap">
			
				<div data-src="../assets/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="text-wrap">
						<div class="text">
							<span class="title">Sage Is</span>
							<span class="sub">Mauris mattis felis eget libero adipiscing elementum.</span>
						</div><!-- .text -->
					</div><!-- .text-wrap -->
					
				</div>
				
				<div data-src="../assets/images/temp/hero/hero-2.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="text-wrap">
						<div class="text">
							<span class="title">Sage Is</span>
							<span class="sub">Mauris mattis felis eget libero adipiscing elementum.</span>
						</div><!-- .text -->
					</div><!-- .text-wrap -->
					
				</div>
			
			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->