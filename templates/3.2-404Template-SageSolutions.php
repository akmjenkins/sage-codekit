<?php $bodyclass = 'error404'; ?>
<?php include('inc/i-header.php'); ?>
			
			<?php include('inc/i-hero-inside.php'); ?>
			
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite home-sm">Sage Solutions Home</a>
					<a href="#">404 Error</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<div class="body">
					
				<div class="header">
					<div class="sw">
						<h1>404 Error</h1>
						<span class="subtitle">Lorem Ipsum Dolar Sit Amet</span>
					</div><!-- .sw -->
				</div><!-- .header -->
				
				<div class="sw cf">
					<div class="main-body">
						<div class="article-body center">
						
							<p>
								The page you're looking for seems to be missing but no need to worry.
							</p>
							
							<p>You can search our site to try and find what you're looking for, click the back button to return to the page you were on previously, or try using the navigation above.</p>
							
							<p>
								If you keep getting this error page, please <a href="#">contact us</a> and let us know.
							</p>
							
							<br />
							<br />
							
							<form action="/" method="get" class="single-form search-form">
								<fieldset>
									<input type="text" name="s" placeholder="Search news...">
									<input type="hidden" name="post_type" value="news">
									<button type="submit" class="sprite-after abs search-hover" title="Search News">Search News</button>
								</fieldset>
							</form><!-- .single-form.search-form -->
							
							<a href="#" class="button green">Go Back</a>
						
						</div><!-- .article-body -->
					</div><!-- .main-body -->
				</div><!-- .sw.cf -->
				
			</div><!-- .body -->
			
<?php include('inc/i-footer.php'); ?>