<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
			
			<?php include('inc/i-hero-inside.php'); ?>
			
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite home-sm">Sage Solutions Home</a>
					<a href="#">The Latest</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<div class="body">
					
				<div class="header">
					<div class="sw">
						<h1>The Latest</h1>
						<span class="subtitle">Lorem Ipsum Dolar Sit Amet</span>
					</div><!-- .sw -->
				</div><!-- .header -->
				
				<div class="sw cf">
					<div class="main-body">
						<div class="article-body">
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempus faucibus ante. Donec eget eleifend justo. 
								Nullam vel dui elit. Nam molestie vestibulum sollicitudin. In quis ex pellentesque, feugiat dolor eu, tincidunt sapien. 
								Aliquam viverra venenatis augue at vestibulum. Sed bibendum nibh a neque accumsan, convallis convallis turpis 
								lacinia. Quisque aliquet arcu sit amet tortor efficitur volutpat. Maecenas interdum velit ultrices ligula ultricies tincidunt. 
							</p>
							
						</div><!-- .article-body -->
					</div><!-- .main-body -->
				</div><!-- .sw -->
				
				<!-- latest-news -->
				<div class="on-white blocks">
					<div class="sw">
					
						<div class="action-bar">
							<span class="h1-style">Latest News</span>
							
							<a href="#" class="button green view-all">View All News</a>
						</div><!-- .action-bar -->

						<div class="grid-wrap">
							<div class="grid eqh collapse-no-flex">
								<div class="col-3 block-col">
									<div class="item">
									
										<a class="block with-img with-meta" href="#">
											<div class="img-wrap">
												<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
											</div><!-- .img-wrap -->
											<div class="content">
												<span class="title">Vivamus Consectetur Pellentesque</span>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
											<div class="meta">
												<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
												<span class="button green category">Category</span>
											</div><!-- .meta -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col-3 -->

								<div class="col-3 block-col">
									<div class="item">
									
										<a class="block with-img with-meta" href="#">
											<div class="img-wrap">
												<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-2.jpg);"></div>
											</div><!-- .img-wrap -->
											<div class="content">
												<span class="title">Vivamus Consectetur Pellentesque</span>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
											<div class="meta">
												<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
												<span class="button green category">Category</span>
											</div><!-- .meta -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col-3 -->

								<div class="col-3 block-col">
									<div class="item">
									
										<a class="block with-img with-meta" href="#">
											<div class="img-wrap">
												<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-3.jpg);"></div>
											</div><!-- .img-wrap -->
											<div class="content">
												<span class="title">Vivamus Consectetur Pellentesque</span>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
											<div class="meta">
												<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
												<span class="button green category">Category</span>
											</div><!-- .meta -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col-3 -->
							</div><!-- .grid -->
						</div><!-- .grid-wrap -->

						
					</div><!-- .sw -->
				</div><!-- ..blocks.on-white -->

				<!-- latest-events -->
				<div class="on-white blocks">
					<div class="sw">
					
						<div class="action-bar">
							<span class="h1-style">Latest Events</span>
							
							<a href="#" class="button green view-all">View All Events</a>
						</div><!-- .action-bar -->

						<div class="grid-wrap">
							<div class="grid eqh collapse-no-flex">
								<div class="col-3 block-col">
									<div class="item">
									
										<a class="block with-img with-meta" href="#">
											<div class="img-wrap">
												<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-1.jpg);"></div>
											</div><!-- .img-wrap -->
											<div class="content">
												<span class="title">Vivamus Consectetur Pellentesque</span>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
											<div class="meta">
												<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
												<span class="button green category">Category</span>
											</div><!-- .meta -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col-3 -->

								<div class="col-3 block-col">
									<div class="item">
									
										<a class="block with-img with-meta" href="#">
											<div class="img-wrap">
												<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-2.jpg);"></div>
											</div><!-- .img-wrap -->
											<div class="content">
												<span class="title">Vivamus Consectetur Pellentesque</span>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
											<div class="meta">
												<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
												<span class="button green category">Category</span>
											</div><!-- .meta -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col-3 -->

								<div class="col-3 block-col">
									<div class="item">
									
										<a class="block with-img with-meta" href="#">
											<div class="img-wrap">
												<div class="img" style="background-image: url(../assets/bin/images/temp/services/service-3.jpg);"></div>
											</div><!-- .img-wrap -->
											<div class="content">
												<span class="title">Vivamus Consectetur Pellentesque</span>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
												<span class="button green">Read More</span>
											</div><!-- .content -->
											<div class="meta">
												<time datetime="2013-10-24" class="meta-text sprite-before calendar-sm">September 24, 2013</time>
												<span class="button green category">Category</span>
											</div><!-- .meta -->
										</a><!-- .block -->
										
									</div><!-- .item -->
								</div><!-- .col-3 -->
							</div><!-- .grid -->
						</div><!-- .grid-wrap -->

						
					</div><!-- .sw -->
				</div><!-- ..blocks.on-white -->
				
			</div><!-- .body -->
			
			<?php include('inc/i-how-can-we-help.php'); ?>
			
<?php include('inc/i-footer.php'); ?>