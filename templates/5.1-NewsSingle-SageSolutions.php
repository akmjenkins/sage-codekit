<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
			
			<?php include('inc/i-hero-inside.php'); ?>
			
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite home-sm">Sage Solutions Home</a>
					<a href="#">The Latest</a>
					<a href="#">News</a>
					<a href="#">News Item One</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<div class="body">
				<article>
					
					<div class="header">
						<div class="sw">
							<h1>News Item One (h1)</h1>
							<span class="subtitle">Lorem Ipsum Dolar Sit Amet</span>
						</div><!-- .sw -->
					</div><!-- .header -->
					
					<div class="sw cf">
						<div class="main-body with-sidebar">
							<div class="article-body">
							
								<div class="article-head">
									<time pubdate datetime="2014-09-14" class="meta sprite-before calendar-sm">September 14, 2014</time>
									
									<div class="categories">
										<a href="#" class="category button green">Category</a>
									</div><!-- .categories -->
									
								</div><!-- .article-head -->
								
								<p class="excerpt">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempus faucibus ante. Donec eget eleifend justo. Nullam vel dui elit. Nam molestie vestibulum 
									sollicitudin. In quis ex pellentesque, feugiat dolor eu, tincidunt sapien. Aliquam viverra venenatis augue at vestibulum. Sed bibendum nibh a neque accumsan, 
									convallis convallis turpis lacinia. Quisque aliquet arcu sit amet tortor efficitur volutpat. Maecenas interdum velit ultrices ligula ultricies tincidunt.
								</p>
								
								<h2>This is a heading (h2)</h2>
								<p>Donec a hendrerit urna. Phasellus id nulla orci. Cras sed blandit enim. Nulla iaculis facilisis vene
								natis. Phasellus sed pharetra libero. Aenean feugiat felis vel urna tristique, id scelerisque nibh rhoncus. 
								Suspendisse feugiat, quam non fringilla aliquam, lacus purus imperdiet quam, sollicitudin dapibus turpis turpis 
								vel purus. Nulla id commodo mauris. Nunc eget tincidunt lectus, sed fermen tum purus. Donec egestas mauris sed nisi 
								convallis, id rhoncus lacus mollis. Fusce tempus urna ac erat tincidunt, nec dictum ante blandit. Ut eget nulla imperdiet, 
								semper purus vel, bibendum eros. Donec vel accumsan diam, non suscipit velit.</p>
								
								<ul>
									<li>In gravida bibendum metus sed iaculis donec facilisis iaculis sem.</li>
									<li>Donec porttitor, arcu id blandit blandit, eros massa euismod arcu, tempor facilisis nisl ligula vel elit.</li>
									<li>
										In gravida bibendum metus sed iaculis donec facilisis iaculis sem.
										<ul>
											<li>In gravida bibendum metus sed iaculis donec facilisis iaculis sem.</li>
										</ul>
									</li>
									<li>In gravida bibendum metus sed iaculis donec facilisis iaculis sem.</li>
								</ul>
								
								<ol>
									<li>In gravida bibendum metus sed iaculis donec facilisis iaculis sem.</li>
									<li>Donec porttitor, arcu id blandit blandit, eros massa euismod arcu, tempor facilisis nisl ligula vel elit.</li>
									<li>
										In gravida bibendum metus sed iaculis donec facilisis iaculis sem.
										<ol>
											<li>In gravida bibendum metus sed iaculis donec facilisis iaculis sem.</li>
										</ol>
									</li>
									<li>In gravida bibendum metus sed iaculis donec facilisis iaculis sem.</li>
								</ol>
								
								
								<h3>This is a subheading (h3)</h3>
								<p>Integer aliquam leo et egestas consequat. Ut euismod dolor nisi, tempus molestie odio porttitor a. In tincidunt elementum pulvinar. 
								Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus vel dictum nisl, vulputate consequat mi. 
								Pellentesque accumsan velit in dictum cursus. Curabitur elementum rhoncus congue. Maecenas non porta mauris, non gravida sapien. Etiam 
								in scelerisque nunc, sit amet aliquet sem.</p>
								
								<h4>Another subheading (h4)</h4>
								<p>In elementum nisl lorem, sed lobortis mi vestibulum sit amet. Vivamus lorem magna, posuere vel fermentum nec, elementum nec mi. 
								Etiam vestibulum convallis erat, ut mattis purus facilisis auctor. Sed faucibus ullamcorper congue. Morbi non ullamcorper ex, in posuere turpis.</p>

								<p>Donec vitae tincidunt justo, vitae mollis tortor. Nam nec velit congue ante dictum commodo id a turpis. Nullam fringilla aliquam nibh, ut vulputate 
								diam convallis ac. Suspendisse eget facilisis odio, eget varius dui. Sed viverra, mauris vitae euismod consequat, dui massa rutrum justo, ultricies mat
								tis leo urna ut justo. Ut sed dolor vitae diam vestibulum facilisis eget cursus erat. Pellentesque feugiat, massa a volutpat interdum, turpis justo porta 
								eros, ac blandit nisi nisi gravida orci. In vitae diam vel lorem luctus ornare.</p>

								<p>Curabitur sed leo eu elit commodo sagittis sed ut lectus. Etiam vestibulum odio a arcu ullamcorper pellentesque. In et purus non lacus fringilla 
								venenatis. Donec pretium aliquet purus, eget posuere libero. Mauris faucibus, nulla et fringilla ornare, lectus magna molestie libero, eu rutrum neque 
								sapien eget neque. Sed sit amet tellus nec magna elementum aliquet quis et ex.</p>
								
							</div><!-- .article-body -->
						</div><!-- .main-body -->
						
						<aside class="sidebar">
							
							<?php include('inc/i-contact-box.php'); ?>
							
							<?php include('inc/i-book-callout.php'); ?>
							
							<?php include('inc/i-archives.php'); ?>
							
						</aside><!-- .sidebar -->
						
					</div><!-- .sw -->
					
				</article>
			</div><!-- .body -->
			
			<?php include('inc/i-how-can-we-help.php'); ?>
			
<?php include('inc/i-footer.php'); ?>