<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
			
			<?php include('inc/i-hero-inside.php'); ?>
			
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite home-sm">Sage Solutions Home</a>
					<a href="#">The Latest</a>
					<a href="#">Events</a>
					<a href="#">Event One</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<div class="body">
				<article>
					
					<div class="header">
						<div class="sw">
							<h1>Event One (h1)</h1>
							<span class="subtitle">Lorem Ipsum Dolar Sit Amet</span>
						</div><!-- .sw -->
					</div><!-- .header -->
					
					<div class="sw cf">
						<div class="main-body with-sidebar">
							<div class="article-body">
							
								<div class="article-head">
									<time datetime="2014-09-14" class="meta sprite-before calendar-sm">September 14, 2014</time>
									<span class="meta sprite-before clock-sm">7:00pm - 9:00pm</span>
									<span class="meta sprite-before marker-sm">This Place, This Town</span>
									<span class="button green" id="view-map">View Map</span>
								</div><!-- .article-head -->
								
								<div class="gmap event-map">
									<div class="map" data-center="47.524755,-52.793752" data-zoom="15" data-markers='[{"title":"JAC","position":"47.524755,-52.793752"}]'></div>
								</div><!-- .gmap -->
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempus faucibus ante. Donec eget eleifend justo. 
								Nullam vel dui elit. Nam molestie vestibulum sollicitudin. In quis ex pellentesque, feugiat dolor eu, tincidunt sapien. 
								Aliquam viverra venenatis augue at vestibulum. Sed bibendum nibh a neque accumsan, convallis convallis turpis 
								lacinia. Quisque aliquet arcu sit amet tortor efficitur volutpat. Maecenas interdum velit ultrices ligula ultricies tincidunt.</p>

								<p>Curabitur sed leo eu elit commodo sagittis sed ut lectus. Etiam vestibulum odio a arcu ullamcorper pellentesque. 
								In et purus non lacus fringilla venenatis. Donec pretium aliquet purus, eget posuere libero. Mauris faucibus, nulla et 
								fringilla ornare, lectus magna molestie libero, eu rutrum neque sapien eget neque. Sed sit amet tellus nec magna 
								elementum aliquet quis et ex.</p>

								<p>Aliquam viverra venenatis augue at vestibulum. Sed bibendum nibh a neque accumsan, convallis convallis turpis 
								lacinia. Quisque aliquet arcu sit amet tortor efficitur volutpat. Maecenas interdum velit ultrices ligula ultricies tincidunt.</p>

								<p>Donec pretium aliquet purus, eget posuere libero. Mauris faucibus, nulla et fringilla ornare, lectus magna molestie libero.</p>
								
							</div><!-- .article-body -->
						</div><!-- .main-body -->
						
						<aside class="sidebar">
							
							<?php include('inc/i-contact-box.php'); ?>
							
							<?php include('inc/i-book-callout.php'); ?>
							
							<?php include('inc/i-archives.php'); ?>
							
						</aside><!-- .sidebar -->
						
					</div><!-- .sw -->
					
				</article>
			</div><!-- .body -->
			
			<?php include('inc/i-how-can-we-help.php'); ?>
			
<?php include('inc/i-footer.php'); ?>