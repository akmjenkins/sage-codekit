<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
			
			<?php include('inc/i-hero-inside.php'); ?>
			
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite home-sm">Sage Solutions Home</a>
					<a href="#">How Can We Help</a>
					<a href="#">Take Care of Yourself</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<div class="body">
				<article>
					
					<div class="header">
						<div class="sw">
							<h1>Take Care of Yourself</h1>
							<span class="subtitle">Lorem Ipsum Dolar Sit Amet</span>
						</div><!-- .sw -->
					</div><!-- .header -->
					
					<div class="sw cf">
						<div class="main-body with-sidebar">
							<div class="article-body">
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempus faucibus ante. Donec eget eleifend justo. Nullam vel dui elit. Nam molestie vestibulum sollicitudin. In quis ex pellentesque, feugiat dolor eu, tincidunt sapien. Aliquam viverra venenatis augue at vestibulum. Sed bibendum nibh a neque accumsan, convallis convallis turpis lacinia. Quisque aliquet arcu sit amet tortor efficitur volutpat. Maecenas interdum velit ultrices ligula ultricies tincidunt.</p>
 
								<p>Aenean sit amet urna malesuada massa efficitur tristique. Etiam tempus tortor ut mauris fringilla fringilla. Phasellus ornare eros luctus velit venenatis gravida. Cras imperdiet vulputate erat, sed bibendum neque rutrum in. Aliquam convallis pulvinar velit non vestibulum. Phasellus placerat efficitur neque, in rutrum metus rhoncus non.</p>
								
							</div><!-- .article-body -->
						</div><!-- .main-body -->
						
						<aside class="sidebar">
							
							<?php include('inc/i-contact-box.php'); ?>
							
							<?php include('inc/i-book-callout.php'); ?>
							
						</aside><!-- .sidebar -->
						
					</div><!-- .sw -->
					
				</article>
				
				<?php include('inc/i-how-can-we-help.php'); ?>
				
				<section class="related">
					<div class="sw">
						<div class="grid collapse-700">
							<div class="col-2 col">	
								<div class="blocks list-view on-white always-list">
								
									<div class="action-bar">
									
										<div class="controls">
											<button class="control sprite-after abs arr-sm-left-white prev">Prev</button>
											<button class="control sprite-after abs arr-sm-right-white next">Next</button>
										</div><!-- .controls -->
									
										<div class="count">
											<span class="num">4</span> Related Services
										</div><!-- .count -->
										
									</div><!-- .action-bar -->
									
									<div class="swiper-wrapper">
										<div class="swipe">
											<div class="swipe-wrap">
												<div>
												
													<div class="grid-wrap">
														<div class="grid fill eqh collapse-no-flex nopad">
														
															<div class="col-3 col">
																<div class="item">
																
																	<a class="block" href="#">
																		<div class="content">
																			<h5>Service One</h5>
																			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																			<span class="button green">Learn More</span>
																		</div><!-- .content -->
																	</a><!-- .block -->
																	
																</div><!-- .item -->
															</div><!-- .col-3 -->
															
															<div class="col-3 col">
																<div class="item">
																
																	<a class="block" href="#">
																		<div class="content">
																			<h5>Service Two</h5>
																			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																			<span class="button green">Learn More</span>
																		</div><!-- .content -->
																	</a><!-- .block -->
																	
																</div><!-- .item -->
															</div><!-- .col-3 -->
															
															<div class="col-3 col">
																<div class="item">
																
																	<a class="block" href="#">
																		<div class="content">
																			<h5>Service Three</h5>
																			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																			<span class="button green">Learn More</span>
																		</div><!-- .content -->
																	</a><!-- .block -->
																	
																</div><!-- .item -->
															</div><!-- .col-3 -->
														
													</div><!-- .grid -->
												</div><!-- .grid-wrap -->

												</div>
												<div>
												
													<div class="grid-wrap">
														<div class="grid fill eqh collapse-no-flex nopad">
														
															<div class="col-3 col">
																<div class="item">
																
																	<a class="block" href="#">
																		<div class="content">
																			<h5>Service One</h5>
																			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																			<span class="button green">Learn More</span>
																		</div><!-- .content -->
																	</a><!-- .block -->
																	
																</div><!-- .item -->
															</div><!-- .col-3 -->
														
													</div><!-- .grid -->
												</div><!-- .grid-wrap -->

												</div>
											</div><!-- .swipe-wrap -->
										</div><!-- .swipe -->
									</div><!-- .swiper-wrapper -->

								</div><!-- .blocks -->
							</div><!-- .col-2 -->
							<div class="col-2 col">					
								<div class="blocks list-view on-white always-list">
								
									<div class="action-bar">
									
										<div class="controls">
											<button class="control sprite-after abs arr-sm-left-white prev">Prev</button>
											<button class="control sprite-after abs arr-sm-right-white next">Next</button>
										</div><!-- .controls -->
									
										<div class="count">
											<span class="num">2</span> Related Resources
										</div><!-- .count -->
										
									</div><!-- .action-bar -->
									
									<div class="swiper-wrapper">
										<div class="swipe">
											<div class="swipe-wrap">
												<div>
												
													<div class="grid-wrap">
														<div class="grid fill eqh collapse-no-flex nopad">
														
															<div class="col-3 col">
																<div class="item">
																
																	<a class="block" href="#">
																		<div class="content">
																			<h5>Resource One</h5>
																			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																			<span class="button green">Learn More</span>
																		</div><!-- .content -->
																	</a><!-- .block -->
																	
																</div><!-- .item -->
															</div><!-- .col-3 -->
															
															<div class="col-3 col">
																<div class="item">
																
																	<a class="block" href="#">
																		<div class="content">
																			<h5>Resource Two</h5>
																			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
																			<span class="button green">Learn More</span>
																		</div><!-- .content -->
																	</a><!-- .block -->
																	
																</div><!-- .item -->
															</div><!-- .col-3 -->
														
													</div><!-- .grid -->
												</div><!-- .grid-wrap -->

												</div>
											</div><!-- .swipe-wrap -->
										</div><!-- .swipe -->
									</div><!-- .swiper-wrapper -->

								</div><!-- .blocks -->
							</div><!-- .col-2 -->
						</div><!-- .grid -->
					</div><!-- .sw -->
				</section>
				
			</div><!-- .body -->
			
<?php include('inc/i-footer.php'); ?>
