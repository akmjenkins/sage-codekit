<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>
			
			<?php include('inc/i-hero-inside.php'); ?>
			
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite home-sm">Sage Solutions Home</a>
					<a href="#">Book Appointment</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<div class="body">
					
				<div class="header">
					<div class="sw">
						<h1>Book Appointment</h1>
						<span class="subtitle">Lorem Ipsum Dolar Sit Amet</span>
					</div><!-- .sw -->
				</div><!-- .header -->
				
				<div class="sw cf">
					<div class="main-body with-sidebar">
						<div class="article-body">				
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempus faucibus ante. 
								Donec eget eleifend justo. Nullam vel dui elit. Nam molestie vestibulum sollicitudin.
							</p>
						</div><!-- .article-body -->
					</div><!-- .main-body -->
					<aside class="sidebar">
						<?php include('inc/i-contact-box.php'); ?>
					</aside><!-- .sidebar -->
				</div><!-- .sw.cf -->

				<section class="contact-section">
					<div class="sw">
					
						<div class="grid contact-grid">
							<div class="col-1 col">
							
								<form action="/" method="post" class="body-form full">
									<fieldset>
										<div class="grid pad10 collapse-599">
											<div class="col-2 col">
												
												<div class="grid pad5 collapse-450">
													<div class="col-2 col">
														<input type="text" name="fname" placeholder="First Name">		
													</div>
													<div class="col-2 col">
														<input type="text" name="lname" placeholder="Last Name">
													</div>
													<div class="col-2 col">
														<input type="email" name="email" placeholder="Email Address">
													</div>
													<div class="col-2 col">
														<input type="tel" pattern="\d+" name="phone" placeholder="Phone">
													</div>
													<div class="col-1 col">
														<select name="specialty">
															<option value="">Speciality</option>
															<option value="">Speciality 1</option>
															<option value="">Speciality 2</option>
															<option value="">Speciality 3</option>
														</select>
													</div>
												
												</div><!-- .grid -->
											</div><!-- .col-2 -->
											
											<div class="col-2 col">
												<div class="grid">
													<div class="col-1 col">
														<textarea name="message" cols="30" rows="10" placeholder="Message"></textarea>				
													</div>
												</div><!-- .grid -->
											</div><!-- .col-2 -->
										</div><!-- .grid -->
										<button class="right button green" type="submit">Register</button>
									</fieldset>
								</form><!-- .body-form -->

							</div><!-- .col-1 -->
						</div><!-- .grid -->
					
					</div><!-- .sw -->
				</section><!-- .contact-section -->
				
			</div><!-- .body -->
			
			<?php include('inc/i-how-can-we-help.php'); ?>
			
<?php include('inc/i-footer.php'); ?>