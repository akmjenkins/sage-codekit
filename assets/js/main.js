var ns = 'SAGE';
window[ns] = {};

// @codekit-append "scripts/components/placeholder.js"; 
// @codekit-append "scripts/components/anchors.external.popup.js"; 
// @codekit-append "scripts/components/standard.accordion.js"
// @codekit-append "scripts/components/gmap.js"; 
// @codekit-append "scripts/components/custom.select.js"; 

// Used in nav.js
// @codekit-append "scripts/components/raf.polyfill.js

// Used in help.js
// @codekit-append "scripts/plugins/scroll.to.js";
// @codekit-append "scripts/components/tim.microtemplate.js";

// Used in swipe.srf.js
// @codekit-append "scripts/components/swipe.js"; 
// @codekit-append "scripts/components/srf.js"; 

// @codekit-append "scripts/swipe.srf.js"; 
// @codekit-append "scripts/device.pixel.ratio.js"; 
// @codekit-append "scripts/blocks.js"; 
// @codekit-append "scripts/locations.js";
// @codekit-append "scripts/hero.js"; 
// @codekit-append "scripts/nav.js";
// @codekit-append "scripts/help.js";

// Additional Modernizr Tests
// @codekit-append "modernizr_tests/ios.js";


$('#view-map')
	.on('click',function(e) {
		$('div.event-map').each(function() {
			var
				el = $(this),
				map = el.children('div.map').data('map')
				
			el.toggleClass('visible');
			google.maps.event.trigger(map.map,'resize');
			map.map.setCenter(map.mapOptions.center);
		});
	});