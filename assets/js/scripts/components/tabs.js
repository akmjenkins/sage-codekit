(function(context) {
		
		$(document)
			.on('click','.tab-control',function(e) {
				e.preventDefault();
			
				var
					el = $(this),
					wrapper = el.closest('div.tab-wrapper'),
					controls = wrapper.children('div.tab-controls').children('.tab-control'),
					holder = wrapper.children('div.tab-holder'),
					tabs = holder.children('div.tab'),
					tabToShow;
			
				
				if(el.data('selector')) {
					tabToShow = $(el.data('selector'));
				}
				
				tabToShow = (tabToShow && tabToShow.length) ? tabToShow : tabs.eq(el.index());
				
				controls.removeClass('selected').eq(tabToShow.index()).addClass('selected');
				tabs.removeClass('selected');
				tabToShow.addClass('selected');
				
				
			});
	
}(window[ns]));