(function(context) {

		//This file could easily be shortened dramatically......I'm too lazy right now

		//magnific popup
		$(document)
			.on('click','.responsive-popup',function(e) {
			
				e.preventDefault();
				
				var 
					magnificItems,
					items,
					isPartOfGallery,
					el = $(this),
					galleryName = el.data('gallery');
					
					isPartOfGallery = !!galleryName;
			
			})
			.on('click','.popup-youtube, .popup-vimeo, .popup-gmaps',function(e) {
				e.preventDefault();
				
				var 
					magnificItems,
					items,
					isPartOfGallery,
					thisIndex = 0,
					el = $(this),
					galleryName = el.data('gallery');
					
					isPartOfGallery = !!galleryName;
				
				if(isPartOfGallery) {
					magnificItems = [];
					items = $('.popup-youtube,.popup-vimeo,.popup-gmaps').filter(function(i) { return $(this).data('gallery') === galleryName; });
					
					items.each(function(i) { 
						var tel = $(this);
						if(this === el[0]) { thisIndex = i; } 
						magnificItems.push({
							src:this.href,
							type:tel.data('type')
						}); 
						return true; 
					});
				} else {
					magnificItems = {src:this.href};
				}
				
				$.magnificPopup.open({
					items:magnificItems,
					type:'iframe',
					disableOn: 700,
					mainClass: 'mfp-fade',
					removalDelay: 160,
					preloader: true,
					fixedContentPos: true,
					gallery:{enabled:isPartOfGallery}
				},thisIndex);
			})
			.on('click','.popup-inline',function(e) {
				e.preventDefault();
				var html = $($(this).data('mfp-src')).html();
				$.magnificPopup.open({
					items: {
						src:html,
						type:'inline',
					},
					disableOn: 700,
					mainClass: 'mfp-fade',
					removalDelay: 160,
					preloader: true,
					fixedContentPos: true
				});
			})
			.on('click','.popup-ajax',function(e) {
				e.preventDefault();
				var 
					thisIndex,
					el = $(this),
					magnificItems = [],
					classes = el.data('classes') || '',
					galleryName = el.data('gallery'),
					items = $('.popup-ajax').filter(function(i) { return ( (!!galleryName && $(this).data('gallery') === galleryName) || el[0] === this ); });
					
					items.each(function(i) { 
						if(this === el[0]) { thisIndex = i; } 
						magnificItems.push({src:this.href}); 
						return true; 
					});
					
				$.magnificPopup.open({
					items:magnificItems,
					disableOn: 700,
					closeBtnInside:true,
					type: 'ajax',
					mainClass: 'mfp-fade '+classes,
					removalDelay: 160,
					preloader: true,
					gallery:{enabled:true},
					fixedContentPos: true,
					type:'ajax'
				},thisIndex);
			})
			.on('click','.popup-gallery',function(e) {
				e.preventDefault();
				var 
					thisIndex = 0,
					el = $(this),
					rel = el.attr('rel'),
					magnificItems = [],
					galleryName = el.data('gallery'),
					items = $('.popup-gallery').filter(function(i) { return !!galleryName && $(this).data('gallery') === galleryName; });
					
					items.each(function(i) { 
						if(this === el[0]) { thisIndex = i; } 
						magnificItems.push({src:this.href}); 
						return true; 
					});
					
				$.magnificPopup.open({
					items:magnificItems,
					image: {
						titleSrc:function(item) {
							return items.eq(item.index).data('title');
						}
					},
					gallery:{enabled:true},
					type:'image'
				},thisIndex);
					
			});
			
			
		//no public API
		return {};

}(window[ns]));