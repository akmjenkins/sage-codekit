(function(context) {

	//date inputs
	$('span.input.date,span.date-input').each(function() {
	
		var 
			scriptDfd,
			cssDfd = new $.Deferred(),
			buildDatePickers = function() {
				var 
					now  	= (new Date()).setHours(0,0,0,0),
					current	= (new Date()),
					num_calenders = 1;
					current.setMonth(new Date(now).getMonth());
					
				DPOptions = {
					eventName		:	'focus',
					date					:	current,
					current				:	current,
					calendars			:	num_calenders,
					starts				:	0,
					format				:	'b d, Y',
					view					:	'days',
					extraHeight		:	true,
					onBeforeShow	:	function() {
						var 
							el = $(this),
							d = el.data();
					},
					onRender : function(date) {
						var 
							linkedEl,
							linkedElDate,
							el = $(this.el),
							disabled = ( date.valueOf() < now.valueOf() ),
							theClass = '';
						
						theClass += date.valueOf() === now.valueOf() ? ' today' : '';
						theClass += (el.val().length && date.valueOf() == (new Date(el.val())).valueOf()) ? ' datepickerSelected' : '';						
						
						if(el.data('linked')) {
							linkedEl = $(el.data('linked'));
							
							if(linkedEl.data('hasdate')) {
								linkedElDate = linkedEl.DatePickerGetDate();
								if(linkedElDate instanceof Date) {
								
									if(date.valueOf() === linkedElDate.valueOf() && !el.data('hasdate')) {
										theClass += ' linkedDate';
									}
								
									if(linkedEl.data('start')) {
										if(date.valueOf() <= linkedElDate.valueOf()) {
											disabled = true;
										}
										
									} else if(linkedEl.data('finish')) {

										if(date.valueOf() >= linkedElDate.valueOf()) {
											disabled = true;
										}
									
									}
								
								}
							}
							
						}
						
						return { 
							disabled : disabled, 
							className : theClass
						};
					},
					onChange : function(format,dateObj,input) {
						var $input = $(input);
						$input.data('hasdate',true);
						$input.DatePickerSetDate(dateObj);
						$input.val(format).DatePickerHide();
						
						if($input.data('linked')) {
							var linkedEl = $($input.data('linked'));
							if(!linkedEl.data('hasdate')) {
								linkedEl.focus();
							}
						}
						
					}
				};
			
				$('span.input.date,span.date-input').each(function() {
				
					if(Modernizr.touch) {
						
						var
							originalInput = $(this).find('input'),
							newInput = originalInput.clone();
						
						newInput.attr('type','date');
						originalInput.replaceWith(newInput);
						newInput.before('<span class="input-label">'+originalInput.attr('placeholder')+'</span>');
						newInput.on('change',function(e) {
							if(newInput.val().length) {
								newInput.parent().addClass('hasdate');
							} else {
								newInput.parent().removeClass('hasdate');
							}
						});
						return;
					} 
				
					$(this)
						.children()
						.on('keydown',function(e) { if(e.keyCode == 9) { $(this).DatePickerHide(); } })
						.DatePicker(DPOptions);
				});
			
			};
	
		if($.fn.DatePicker || Modernizr.touch) {
			buildDatePickers.apply();
			return false;
		}
	
		//get the script
		scriptDfd = $.getScript(templateJS.templateURL+'/assets/bin/js/libraries/datepicker/datepicker.min.js?'+templateJS.CACHE_BUSTER);
		
		//append the CSS
		$('<link/>')
			.attr('href',templateJS.templateURL+'/assets/bin/js/libraries/datepicker/datepicker.css?'+templateJS.CACHE_BUSTER)
			.attr('rel','stylesheet')
			.attr('type','text/css')
			.appendTo($('head'))
			.on('load',cssDfd.resolve);
		
		
		$.when.apply(this,[scriptDfd,cssDfd]).then(buildDatePickers);
		
		
		return false;
	});
		
}(window[ns]));