//nav
(function(context) {		

	var body = $('body');
	var cb = function(e) { 
		var el = e.target ? e.target : e.srcElement;
		
		if(el.id === 'wrapper') {
			body.removeClass('animating').scrollTop(0); 
		}
	}
	$(document)
		.on('ready',function() {
			body.addClass('loaded');
		})
		.on('click','nav>ul>li>div',function(e) {
			$(this).closest('li').toggleClass('expanded');
		})
		.on('click','#mobile-nav',function(e) {
			e.preventDefault();
			body.toggleClass('show-nav').addClass('animating');
			return false;
		});
	
	body
		.on({
			'webkitTransitionEnd': cb,
			'msTransitionEnd': cb,
			'oTransitionEnd': cb,
			'otransitionend': cb,
			'transitionend': cb
		});
		
		
}(window[ns]));