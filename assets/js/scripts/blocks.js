(function(context) {

	$(window)
		.on('load resize',function() {
			var blocks = $('.blocks').filter(function() { return !$(this).hasClass('always-list'); });
			
			if(window.outerWidth < 650) {
				blocks.addClass('list-view');
			} else {
				blocks.removeClass('list-view');
			}
		}).trigger('resize');
		
	var checkBlockControls = function() {
		var el = $(this);
		
		el.find('div.swipe').each(function() {
			var swiper = $(this).data('swipe');
			
			if(swiper && swiper.getNumSlides() > 1) {
				el.addClass('with-swiper');
				
				el.on('click','button.control',function() {
					var pos = $(this).hasClass('next') ? 1 : -1;
					swiper.slide(swiper.getPos()+pos);
				});
				
			} else {
				el.removeClass('with-swiper');
			}
		});
	};

	$('.blocks')
			.each(function() {
				checkBlockControls.apply(this);
			})
			.on('gridReloaded',function() {
				checkBlockControls.apply(this);
			});
		
}(window[ns]));