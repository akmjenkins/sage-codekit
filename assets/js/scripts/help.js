(function(ScrollTo,SwipeSRF) {
		
	var 
		loadingProcess = false, //jqXHR
		help = $('section.how-can-we-help'),
		answersContainer = help.find('.how-can-we-help-results'),
		answersError = answersContainer.find('div.error'),
		servicesContainer = help.find('.how-can-we-help-services'),
		servicesButtons = help.find('button.filter'),
		servicesSwiper = servicesContainer.find('div.services-swiper'),
		services = servicesContainer.find('div.services-source .service'),
		servicesCount = servicesContainer.find('div.count span.num'),
		tabs = help.find('div.tab'),
		tabSwiperEl = help.find('div.tab-swiper'),
		tabSwiper = tabSwiperEl.data('swipe'),
		methods = {
		
			cancelLoading: function() {
				loadingProcess && loadingProcess.abort();
			},
		
			toggleExpand: function() {
				var isExpanded = help.hasClass('expanded');
				if(isExpanded) {
					help.removeClass('expanded');
				} else {
					help.addClass('expanded');
					tabSwiper.setup();
				}
			},
			
			doneLoadingQuestion: function(response) {
				answersContainer.find('div.answers').html(response);
			},
			
			failedLoadingQuestion: function() {
				//show a failed loading message
				answersError.removeClass('i-hidden');
			},
			
			loadQuestion: function(url,q) {			
				var self = this;
				this.showLoading(true);
				
				answersError.addClass('i-hidden');
				answersContainer.find('div.answers').html('');
				
				
				loadingProcess = $.ajax({
					type: 'get',
					url:url,
					dataType: 'html',
					data: {
						q:q
					}
				})
					.done(function(r,status,jqXHR) {
						if(status === 'success') {
							return self.doneLoadingQuestion(r)
						}
						
						self.failedLoadingQuestion(jqXHR);
					})
					.fail(function(r,status,jqXHR) {
						if(status !== 'abort') {
							self.failedLoadingQuestion();
						}
					})
					.always(function() {
						self.showLoading(false);
					});
				
			},
			
			showLoading: function(show) {
			
			},
			
			filterServices: function() {
				var 
					filteredServiceDivs,
					numFound = 0,
					servicesPerSlide = 5,
					contentHTML = '',
					slideHTML = '',
					filters = [],
					swipeTemplate = '\
						<div class="swiper-wrapper collapse-750">\
							<div class="swipe">\
								<div class="swipe-wrap">\
									{{contents}}\
								</div><!-- .swipe-wrap -->\
							</div><!-- .swipe -->\
						</div><!-- .swiper-wrapper -->',
					contentTemplate = '\
						<div>\
							<div class="grid-wrap">\
								<div class="grid eqh fill collapse-no-flex centered">\
									{{contents}}\
								</div><!-- .grid -->\
							</div><!-- .grid-wrap -->\
						</div>';
						

				servicesSwiper.html('');
				
				//set up filters
				servicesButtons
					.filter(function() { return $(this).hasClass('active') })
					.filter(function() { filters.push($(this).data('slug')); });
					
				//loop through services divs
				filteredServiceDivs = services.filter(function() {
					var
						found = false,
						el = $(this),
						services = el.data('services');
						
						$.each(services,function(i,service) {
							if(filters.indexOf(service) !== -1) {
								found = true;
								numFound++;
								return false;
							}
						});
						
						return found;
				});
				
				for(var i=0;i<Math.ceil(filteredServiceDivs.length/servicesPerSlide);i++) {
					var start = (i*servicesPerSlide);
					slideHTML = '';
					filteredServiceDivs
						.slice(start,start+servicesPerSlide)
						.each(function(i,el) {
							slideHTML += el.outerHTML;
						});
						
					contentHTML += tim(contentTemplate,{contents:slideHTML});
					
				}
				
				servicesCount.html(numFound);
				SwipeSRF.buildSwipe.apply(servicesSwiper.html(tim(swipeTemplate,{contents:contentHTML})).find('div.swipe'));
				servicesSwiper.trigger('gridReloaded');
				
			}
			
		};
		
		
		//toggle the help section
		help
			.on('click','button.toggle',function(e) {
				methods.toggleExpand();
			})
			.on('submit','form',function(e) {
				e.preventDefault();
				var input = $(this).find('input');
				methods.loadQuestion(this.action,input.val());
			});
		
		//filter
		servicesButtons.on('click',function(e) {
			var 
				el = $(this),
				buttonEl = el.children('span.sprite-after'),
				sprite = el.data('sprite'),
				isActive = el.hasClass('active'),
				newClass = sprite + (isActive ? '-g' : ''),
				currentClass = sprite + (isActive ? '' : '-g');
				
				buttonEl.removeClass(currentClass).addClass(newClass);
				
				if(isActive) {
					el.removeClass('active');
				} else {
					el.addClass('active');
				}
				
				methods.filterServices();
				
		});
		
		//highlight the first tab always
		tabs.eq(0).addClass('active');
		
		//show the questions/services divs when the slider has changed
		tabSwiperEl.on('swipeChanged',function(e,i,div) { 
			methods.cancelLoading();
			
			tabs.removeClass('active').eq(i).addClass('active'); 
			
			servicesContainer.addClass('hidden');
			answersContainer.addClass('hidden');
			
			switch(i) {
				case 2: //services
					servicesContainer.removeClass('hidden');
					methods.filterServices();
					break;
				case 1: //question
					answersContainer.removeClass('hidden');
					break;
			}
			
		});
		
		//click listener for the tabs
		tabs.on('click',function(e) {
		
			ScrollTo('div.how-can-we-help-body',{offsetTop:50});
		
			var index = $(this).index();
			if(tabSwiper.getPos() !== index) {
				tabSwiper.slide(index);
			}
		});
	


}(window[ns].ScrollTo,window[ns].SwipeSRF));