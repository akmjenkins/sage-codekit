(function(SwipeSRF,raf) {


	var 
		$document = $(document),
		$body = $('body');

	//mobile nav
	(function() {
	
		var cb = function(e) { 
			var el = e.target ? e.target : e.srcElement;
			
			if($(el).hasClass('page-wrapper')) {
				$body.removeClass('animating');//.scrollTop(0); 
			}
		}
		$document.on('click','#mobile-nav',function(e) {
			e.preventDefault();
			$body.toggleClass('show-nav').addClass('animating');
			return false;
		});
		
		$body
			.on({
				'webkitTransitionEnd': cb,
				'msTransitionEnd': cb,
				'oTransitionEnd': cb,
				'otransitionend': cb,
				'transitionend': cb
			});
		
	}());


	var 
		nav = $('nav'),
		searchArea = $('div.nav div.search-area'),
		bookAppointment = nav.find('div.nav-book-appointment');

	//change to fixed position
	var fixedPositionNav = {
		
		originalPosition: nav.offset().top,
		isProcessing: false,
		lastScrollY: $(window).scrollTop(),
		
		requestProcess: function() {
			if(!this.isProcessing) {
				this.isProcessing = true;
				this.lastScrollY = $(window).scrollTop();
				raf.requestAnimFrame(this.process,this);
			}
		},
		
		process: function() {
			var self = fixedPositionNav;
			if(self.lastScrollY >= self.originalPosition) {
				nav.addClass('fixed');
			} else {
				nav.removeClass('fixed');
			}
			
			self.isProcessing = false;
		}
		
	};
	
	$(window).on('scroll',function() { fixedPositionNav.requestProcess(); });
	//check upon page load the position of the nav
	fixedPositionNav.requestProcess();
	
	//manage book appointment
	bookAppointment.each(function() {
		var el = $(this);
		el.children('button').on('click',function(e) {
			el.toggleClass('expanded');
			
			if(el.hasClass('expanded')) {
				el.find('input').eq(0).focus();
			} else {
				el.find('input,textarea').blur();
			}
			
		});
		
		
		$document
			.on('click',function(e) {
			
				//close the book appointment widget when clicked outside
				if(bookAppointment.hasClass('expanded') && !bookAppointment.find(e.target).length) {
					bookAppointment.removeClass('expanded');
					return;
				}
				
				//close the search box when clicked outside
				if(searchArea.hasClass('expanded') && !searchArea.find(e.target).length) {
					searchArea.removeClass('expanded');
					return;
				}
			});
		
	});
	
	//sliders in nav
	nav
		.on('mouseenter','div.sw>ul>li',function(e) {
			var
				el  =$(this),
				dd = el.children('div'),
				swiperEl = dd.find('div.swipe-to-be'),
				swiper = swiperEl.data('swipe');
				
				if(!swiperEl.hasClass('swipe')) {
					//build for the first time
					SwipeSRF.buildSwipe.apply(swiperEl.addClass('swipe'));
				} else {
					//rebuild
					swiper.setup();
				}
				
				swiperEl.trigger('swipeChanged',[0,swiperEl.find('div.img').eq(0).parent()[0]]);
				
		})
		.on('mouseenter','li li a',function(e) {
			var
				el = $(this),
				index = el.parent().index(),
				dd = el.closest('div'),
				swiper = dd.find('div.swipe');
				
				swiper.data('swipe').slide(index);
		})
		.on('swipeChanged',function(e,index,element) {
			var
				source,
				el = $(element),
				imgEl = el.find('div.img'),
				imgSrc = imgEl.data('img');
				
				if(imgEl.data('working')) { return; }
				
				imgEl.data('working',true);
				
				if(imgSrc) {
					source = templateJS.templateURL+'/'+imgSrc;
					$('<img/>')
						.on('load',function() {
							imgEl
								.css('backgroundImage','url('+source+')')
								.addClass('loaded')
								.removeData('working')
								.removeData('img');
						})
						.on('error',function() {
							imgEl.removeData('working');
						})
						.attr('src',source);
				}
				
		});
		
	//toggle search box
	searchArea.each(function() {
		var input = searchArea.find('input');
		
		searchArea.children('button').on('click',function(e) {
			searchArea.toggleClass('expanded');
			if(searchArea.hasClass('expanded')) {
				input.focus();
			} else {
				input.blur();
			}
		});
	});
		

	//no public API
	return {};

}(window[ns].SwipeSRF,window[ns].raf));