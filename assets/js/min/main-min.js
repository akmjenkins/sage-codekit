var ns = 'SAGE';
window[ns] = {};

// @codekit-append "scripts/components/placeholder.js"; 
// @codekit-append "scripts/components/anchors.external.popup.js"; 
// @codekit-append "scripts/components/standard.accordion.js"
// @codekit-append "scripts/components/gmap.js"; 
// @codekit-append "scripts/components/custom.select.js"; 

// Used in nav.js
// @codekit-append "scripts/components/raf.polyfill.js

// Used in help.js
// @codekit-append "scripts/plugins/scroll.to.js";
// @codekit-append "scripts/components/tim.microtemplate.js";

// Used in swipe.srf.js
// @codekit-append "scripts/components/swipe.js"; 
// @codekit-append "scripts/components/srf.js"; 

// @codekit-append "scripts/swipe.srf.js"; 
// @codekit-append "scripts/device.pixel.ratio.js"; 
// @codekit-append "scripts/blocks.js"; 
// @codekit-append "scripts/locations.js";
// @codekit-append "scripts/hero.js"; 
// @codekit-append "scripts/nav.js";
// @codekit-append "scripts/help.js";

// Additional Modernizr Tests
// @codekit-append "modernizr_tests/ios.js";


$('#view-map')
	.on('click',function(e) {
		$('div.event-map').each(function() {
			var
				el = $(this),
				map = el.children('div.map').data('map')
				
			el.toggleClass('visible');
			google.maps.event.trigger(map.map,'resize');
			map.map.setCenter(map.mapOptions.center);
		});
	});

(function(context) {
	/* jquery placeholder plugin */
	if(!Modernizr.placeholder) {
		(function($){$.Placeholder={settings:{color:"rgb(169,169,169)",dataName:"original-font-color"},init:function(c){if(c){$.extend($.Placeholder.settings,c)}var d=function(a){return $(a).val()};var e=function(a,b){$(a).val(b)};var f=function(a){return $(a).attr("placeholder")};var g=function(a){var b=d(a);return(b.length===0)||b==f(a)};var h=function(a){$(a).data($.Placeholder.settings.dataName,$(a).css("color"));$(a).css("color",$.Placeholder.settings.color)};var i=function(a){$(a).css("color",$(a).data($.Placeholder.settings.dataName));$(a).removeData($.Placeholder.settings.dataName)};var j=function(a){e(a,f(a));h(a)};var k=function(a){if($(a).data($.Placeholder.settings.dataName)){e(a,"");i(a)}};var l=function(){if(g(this)){k(this)}};var m=function(){if(g(this)){j(this)}};var n=function(){if(g(this)){k(this)}};$("textarea, input[type='text'],input[type='email'],input[type='tel'],input[type='search']").each(function(a,b){if($(b).attr("placeholder")){$(b).focus(l);$(b).blur(m);$(b).bind("parentformsubmitted",n);$(b).trigger("blur");$(b).parents("form").submit(function(){$(b).trigger("parentformsubmitted")})}});return this}}})(jQuery);
		$(document).ready(function() { $.Placeholder.init(); });
	}
}(window[ns]));

//anchor listeners
(function(context) {

	//pub/sub listeners on the document
	$(document)
	
		//link events
		.on('click','a[rel=external]',function (e) { 
			e.preventDefault();
			window.open(this.href); 
		})
		.on('click','a[rel=popup]',function () { 
			var 	el 				= $(this),
					name			=	el.data('name') || (new Date()).getTime(),
					opts				=	el.data('opts'),
					optsString		=	'';
					
			//make all popups that don't explicity say so have scrollbars and are resizeable
			if(!opts) {
				opts = {scrollbars : 'yes',resizeable:'yes'};
			}
			
			if(typeof opts !== 'object') { opts = $.parseJSON(opts); }
		
			opts.resizable = (opts.resizable) ? opts.resizable : 'yes';
			opts.scrollbars = (opts.scrollbars) ? opts.scrollbars : 'yes';
		
			for(i in opts) {
				//don't allow a window to open that is taller than the browser
				if(i === 'height') {
					opts[i] = (window.screen.height < (+opts[i])) ? window.screen.height-50 : opts[i];
				} else if(i === 'width') {
					opts[i] = (window.screen.width < (+opts[i])) ? window.screen.width-50 : opts[i];
				}
				
				optsString	+= i+'='+opts[i]+',';
			}

			window.open(this.href, name,optsString);
			return false; 
		});
		
}(window[ns]));

//standard accordion
(function(context) {
	var opts;
	$('div.accordion')
		.on('click','div.accordion-item-handle',function(e) {
			var 
				el = $(this).parent(),
				acc = el.closest('div.accordion');
				
			e.preventDefault();
			
			if(acc.hasClass('inactive') || el.hasClass('inactive')) { return; }
			
			opts = {duration:450};
			
			if(el.hasClass('expanded')) {
				el.removeClass('expanded').children('div.accordion-item-content').stop().slideUp(opts);
			} else {
				el.addClass('expanded').children('div.accordion-item-content').stop().slideDown(opts);
			}
			
			acc.hasClass('allow-multiple') || el.siblings().removeClass('expanded').children('div.accordion-item-content').stop().slideUp(opts);
			
			//no bubble;
			return false;
			
		}).each(function() {
			$(this).find('div.expanded').children('div.accordion-item-content').slideDown(opts);
		});
	
	
	
}(window[ns]));

//Simple Embedded Google Map
(function(context) {

	var loadGMap = function(cb) {	
		google.load('maps',3.15, {
			"base_domain":"google.ca","other_params":"sensor=false",
			"callback": cb
		});
	};

	var loadGoogleLoader = function(cb) {
		$.getScript('https://www.google.com/jsapi')
			.done(cb);
	};
	
	var Map = function(containerEl,options) {
		var self = this;
		
		google.maps.visualRefresh = true;
		
		this.canvas = containerEl;
		this.markers = [];
		var center = containerEl.data('center').split(',');
		
		this.canvas.on('mapLoad',function(e) { 
			containerEl.addClass('loaded'); 
			
			//fit to bounds on markers if necessary
			if(self.markers.length) {
				var 
					mapBounds = self.map.getBounds(),
					markerBounds = new google.maps.LatLngBounds();
					
				$.each(self.markers,function(i,marker) {
					markerBounds.extend(marker.getPosition());
				});
				
				if(!mapBounds.union(markerBounds).equals(mapBounds)) {
					self.map.fitBounds(markerBounds);
				}
				
			}
			
			
		});
		
		this.mapOptions = {
			center: new google.maps.LatLng(+center[0], +center[1]),
			zoom: +containerEl.data('zoom') || 15,
			streetViewControl:false,
			panControl:false,
			draggable: (!Modernizr.touch || $(window).outerWidth() > 600),
			mapTypeControlOptions:{
				style:google.maps.MapTypeControlStyle.DROPDOWN_MENU
			},
			zoomControlOptions:{
				style:google.maps.ZoomControlStyle.SMALL
			},
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		
		this.map = new google.maps.Map(this.canvas[0],this.mapOptions);
		
		var 
			lastMarkerImage,
			lastMarkerIcon,
			markers = containerEl.data('markers');
		$.each(markers,function(i,markerObj) {
			var 
				markerIcon = null,
				center = markerObj.position.split(','),
				createMarker = function() {
					self.markers.push(new google.maps.Marker({
						position: new google.maps.LatLng(+center[0], +center[1]),
						map: self.map,
						title: markerObj.title,
						icon:markerIcon
					}));				
				};
			
			if(markerObj.image) {
				if(lastMarkerImage === markerObj.image) {
					markerIcon = lastMarkerIcon
					return;
				}
				
				$('<img/>')
					.on('load',function() {
						var
							width = this.width,
							height = this.height;
							
						//anchor is center
						markerIcon = { 
							anchor: new google.maps.Point(this.width/2,this.height/2), 
							url: templateJS.templateURL+markerObj.image
						};
						
						lastMarkerImage = markerObj.image;
						lastMarkerIcon = markerIcon;
						createMarker();
					})
					.attr('src',templateJS.templateURL+markerObj.image);
					return;
			}
			
			createMarker();
			
			

		});
		
		this.convertListeners();
		
	};

	
	Map.prototype.convertListeners = function() {
	
		this.mapListeners = {};
	
			var 
				self = this,
				ns = google.maps.event,
				loadListener = ns.addListener(self.map,'tilesloaded',function() { 
					ns.removeListener(loadListener);
					self.canvas.trigger('mapLoad',[self]);
					self.mapListeners.resizeListener = self.mapListeners.resizeListener || ns.addListener(self.map,'resize',function() { self.canvas.trigger('mapResized'); });
					self.mapListeners.idleListener = self.mapListeners.idleListener || ns.addListener(self.map,'idle',function() { 
							var 
								args = {},
								center = self.map.getCenter();
								
								args.lat = center.lat();
								args.lng = center.lng();
								args.zoom = self.map.getZoom();
							
							self.canvas.trigger('mapMoved',[args]); 
					});
					self.mapListeners.zoomListener = self.mapListeners.zoomListener || ns.addListener(self.map,'zoom_changed',function() { self.canvas.trigger('mapZoomed',[self.map.getZoom()]); });
				});
				self.mapListeners.mapTypeListener = self.mapListeners.mapTypeListener || ns.addListener(self.map,'maptypeid_changed',function() { self.canvas.trigger('mapTypeChanged'); });
				
			self.canvas.on('doResizeMap',function() { ns.trigger(self.map,'resize'); });
	
	};

	var 
		hasRequestedGoogle = false,
		googleDefer = $.Deferred(),
		googleRequestComplete = googleDefer.promise();
		
	$('div.map').each(function() {
		var el = this;
		
		var cb = function() {
			var map = new Map($(this));
			window.maps = window.maps || [];
			window.maps.push(map);
			
			$(this).data('map',map);
		}
		
		if(!hasRequestedGoogle) {
			hasRequestedGoogle = true;
		} else {
			return googleRequestComplete.done(function() {
				cb.apply(el);
			});
		}
		
		if(window.google === undefined || window.google.maps === undefined) { 
		
			window.createMap = function() { 
				googleDefer.resolve();
				cb.apply(el); 
			};
			
			return $.getScript('https://maps.googleapis.com/maps/api/js?sensor=false&callback=createMap');
		}
		
		if(window.google === undefined) { return loadGoogleLoader(function() { loadGMap(function() { cb.apply(el); }); }); }
		
		if(window.google.maps === undefined) { return loadGMap(function() { cb.apply(el); }); }
		
		cb.apply(el);
		
	});	
	
}(window[ns]));

(function(context) {

		//selector wrapper
		$('div.selector').each(function() {
			var 
				el = $(this),
				select = $('select',el),
				val = $('span.value',el);
				
			select
				.on('change',function(e) { val.html($(this).children('option:selected').text()); })
				.trigger('change');
		});
		
}(window[ns]));

(function(context) {
	

	var methods = {
		requestAnimFrame: function(callback){
			var raf = (function(){
				  return  window.requestAnimationFrame       ||
						  window.webkitRequestAnimationFrame ||
						  window.mozRequestAnimationFrame    ||
						  function( callback ){
							window.setTimeout(callback, 1000 / 60);
						  };
				})();
				
			raf.apply(window,[callback]);
		},
		
		cancelAnimFrame: function(identifier){
			var caf = (function(){
				  return  window.cancelAnimationFrame       ||
						  window.webkitCancelAnimationFrame ||
						  window.mozCancelAnimationFrame    ||
						  function( idenfitier ){
							clearTimeout(identifier);
						  };
				})();
				
			caf.apply(window,[callback]);
		}
		
	};

	$.extend(context,{
		raf: methods
	});

}(window[ns]));

(function(context) {

	$.fn.scrollTo = function( target, options, callback ){

		if(typeof options == 'function' && arguments.length == 2){ 
			callback = options; options = target; 
		}
		
		var settings = $.extend({
			scrollTarget: target,
			offsetTop: 50,
			duration: 500,
			easing: 'swing'
		}, options);
		
		return this.each(function(){
			var scrollPane = $(this);
			var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
			var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top - parseInt(settings.offsetTop);
			scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
				if (typeof callback == 'function') { callback.call(this); }
			});
		});
	}
	

	$.extend(context,{
		ScrollTo: function() {
			var $body = $('body');
			$body.scrollTo.apply($body,arguments);			
		}
	});

}(window[ns]));

/*!
* Tim
*   github.com/premasagar/tim
*
*//*
    A tiny, secure JavaScript micro-templating script.
*//*

    by Premasagar Rose
        dharmafly.com

    license
        opensource.org/licenses/mit-license.php

    **

    creates global object
        tim

    **

    v0.3.0

*//*global window */

/*
    TODO:
    * a way to prevent a delimiter (e.g. ", ") appearing last in a loop template
    * Sorted constructor for auto-sorting arrays - used for parsers -> two parsers are added, one for identifying and parsing single-tokens and one for open/close tokens - the parsers then create two new Sorted instance, one for single-token plugins and one for open/close token plugins
*/

(function(name, definition, context) {
    if (typeof module != 'undefined' && module.exports) {
        module.exports = definition();
    } else if (typeof context['define'] == 'function' && context['define']['amd']) {
        define(definition);
    } else {
        context[name] = definition();
    }
})('tim', function() {

    var tim = (function createTim(initSettings){
        "use strict";
        
        var settings = {
                start: "{{",
                end  : "}}",
                path : "[a-z0-9_$][\\.a-z0-9_]*" // e.g. config.person.name
            },
            templates = {},
            filters = {},
            stopThisFilter, pattern, initialized, undef;
            
            
        /////
        

        // Update cached regex pattern
        function patternCache(){
            pattern = new RegExp(settings.start + "\\s*("+ settings.path +")\\s*" + settings.end, "gi");
        }
        
        // settingsCache: Get and set settings
        /*
            Example usage:
            settingsCache(); // get settings object
            settingsCache({start:"<%", end:"%>", attr:"id"}); // set new settings
        */
        function settingsCache(newSettings){
            var s;
        
            if (newSettings){
                for (s in newSettings){
                    if (newSettings.hasOwnProperty(s)){
                        settings[s] = newSettings[s];
                    }
                }
                patternCache();
            }
            return settings;
        }
            
        // Apply custom settings
        if (initSettings){
            settingsCache(initSettings);
        }
        else {
            patternCache();
        }
        
        
        /////
        
        
        // templatesCache: Get and set the templates cache object
        /*
            Example usage:
            templatesCache("foo"); // get template named "foo"
            templatesCache("foo", "bar"); // set template named "foo" to "bar"
            templatesCache("foo", false); // delete template named "foo"
            templatesCache({foo:"bar", blah:false}); // set multiple templates
            templatesCache(false); // delete all templates
        */
        function templatesCache(key, value){
            var t;
        
            switch (typeof key){
                case "string":
                    if (value === undef){
                        return templates[key] || "";
                    }
                    else if (value === false){
                        delete templates[key];
                    }
                    else {
                        templates[key] = value;
                    }
                break;
                
                case "object":
                    for (t in key){
                        if (key.hasOwnProperty(t)){
                            templatesCache(t, key[t]);
                        }
                    }
                break;
                
                case "boolean":
                if (!key){
                    templates = {};
                }
                break;
            }
            return templates;
        }
        
        function extend(obj1, obj2){
            var key;
            for (key in obj2){
                if (obj2.hasOwnProperty(key)){
                    obj1[key] = obj2[key];
                }
            }
            return obj1;
        }
        
        
        /////
        
        
        // FILTERS    
        function sortByPriority(a, b){
            return a[1] - b[1];
        }
        
        // Add filter to the stack
        function addFilter(filterName, fn, priority){
            var fns = filters[filterName];
            if (!fns){
                fns = filters[filterName] = [];
            }
            fns.push([fn, priority || 0]);
            fns.sort(sortByPriority);
            return fn;
        }
        
        function applyFilter(filterName, payload){
            var fns = filters[filterName],
                args, i, len, substituted;
                
            if (fns){
                args = [payload];
                i = 2;
                len = arguments.length;            
                for (; i < len; i++){
                    args.push(arguments[i]);
                }

                i = 0;
                len = fns.length;
                for (; i < len; i++){
                    args[0] = payload;
                    substituted = fns[i][0].apply(null, args);
                    if (payload !== undef && substituted !== undef){
                        payload = substituted;
                    }
                    if (substituted === null){
                        payload = '';
                    }
                    if (stopThisFilter){
                        stopThisFilter = false;
                        break;
                    }
                }
            }
            return payload;
        }
        
        // Router for adding and applying filters, for Tim API
        function filter(filterName, payload){
            return (typeof payload === "function" ? addFilter : applyFilter)
                .apply(null, arguments);
        }
        filter.stop = function(){
            stopThisFilter = true;
        };
        
        
        /////
        
        
        // Merge data into template
        /*  
            // simpler alternative, without support for iteration:
            template = template.replace(pattern, function(tag, token){
                return applyFilter("token", token, data, template);
            });
        */
        // TODO: all an array to be passed to tim(), so that the template is called for each element in it
        function substitute(template, data){
            var match, tag, token, substituted, startPos, endPos, templateStart, templateEnd, subTemplate, closeToken, closePos, key, loopData, loop;
        
            while((match = pattern.exec(template)) !== null) {
                token = match[1];
                substituted = applyFilter("token", token, data, template);
                startPos = match.index;
                endPos = pattern.lastIndex;
                templateStart = template.slice(0, startPos);
                templateEnd = template.slice(endPos);
                
                // If the final value is a function call it and use the returned
                // value in its place.
                if (typeof substituted === "function") {
                    substituted = substituted.call(data);
                }
                
                if (typeof substituted !== "boolean" && typeof substituted !== "object"){
                    template = templateStart + substituted + templateEnd;
                } else {
                    subTemplate = "";
                    closeToken = settings.start + "/" + token + settings.end;
                    closePos = templateEnd.indexOf(closeToken);
                    
                    if (closePos >= 0){
                        templateEnd = templateEnd.slice(0, closePos);
                        if (typeof substituted === "boolean") {
                            subTemplate = substituted ? templateEnd : '';
                        } else {
                            for (key in substituted){
                                if (substituted.hasOwnProperty(key)){
                                    pattern.lastIndex = 0;
                                
                                    // Allow {{_key}} and {{_content}} in templates
                                    loopData = extend({_key:key, _content:substituted[key]}, substituted[key]);
                                    loopData = applyFilter("loopData", loopData, loop, token);
                                    loop = tim(templateEnd, loopData);
                                    subTemplate += applyFilter("loop", loop, token, loopData);
                                }
                            }
                            subTemplate = applyFilter("loopEnd", subTemplate, token, loopData);
                        }
                        template = templateStart + subTemplate + template.slice(endPos + templateEnd.length + closeToken.length);
                    }
                    else {
                        throw "tim: '" + token + "' not closed";
                    }
                }
                
                pattern.lastIndex = 0;
            }
            return template;
        }
        
        
        // TIM - MAIN FUNCTION
        function tim(template, data){
            var templateLookup;
        
            // On first run, call init plugins
            if (!initialized){
                initialized = 1;        
                applyFilter("init");
            }
            template = applyFilter("templateBefore", template);
        
            // No template tags found in template
            if (template.indexOf(settings.start) < 0){
                // Is this a key for a cached template?
                templateLookup = templatesCache(template);
                if (templateLookup){
                    template = templateLookup;
                }
            }
            template = applyFilter("template", template);
            
            // Substitute tokens in template
            if (template && data !== undef){
                template = substitute(template, data);
            }
            
            template = applyFilter("templateAfter", template);
            return template;
        }
        
        // Get and set settings, e.g. tim({attr:"id"});
        tim.settings = settingsCache;
        
        // Get and set cached templates
        tim.templates = templatesCache;
        
        // Create new Tim function, based on supplied settings, if any
        tim.parser = createTim;
        
        // Add new filters and trigger existing ones. Use tim.filter.stop() during processing, if required.
        tim.filter = filter;
        
        
        /////
        
        
        // dotSyntax default plugin: uses dot syntax to parse a data object for substitutions
        addFilter("token", function(token, data, tag){
            var path = token.split("."),
                len = path.length,
                dataLookup = data,
                i = 0;

            for (; i < len; i++){
                dataLookup = dataLookup[path[i]];
                
                // Property not found
                if (dataLookup === undef){
                    throw "tim: '" + path[i] + "' not found" + (i ? " in " + tag : "");
                }
                
                // Return the required value
                if (i === len - 1){
                    return dataLookup;
                }
            }
        });
        
        
        /////
        
        
        // Dom plugin: finds micro-templates in <script>'s in the DOM
        // This block of code can be removed if unneeded - e.g. with server-side JS
        // Default: <script type="text/tim" class="foo">{{TEMPLATE}}</script>
        if (window && window.document){
            tim.dom = function(domSettings){
                domSettings = domSettings || {};
                
                var type = domSettings.type || settings.type || "text/tim",
                    attr = domSettings.attr || settings.attr || "class",
                    document = window.document,
                    hasQuery = !!document.querySelectorAll,
                    elements = hasQuery ?
                        document.querySelectorAll(
                            "script[type='" + type + "']"
                        ) :
                        document.getElementsByTagName("script"),
                    i = 0,
                    len = elements.length,
                    elem, key,
                    templatesInDom = {};
                    
                for (; i < len; i++){
                    elem = elements[i];
                    // Cannot access "class" using el.getAttribute()
                    key = attr === "class" ? elem.className : elem.getAttribute(attr);
                    if (key && (hasQuery || elem.type === type)){
                        templatesInDom[key] = elem.innerHTML;
                    }
                }
                
                templatesCache(templatesInDom);
                return templatesInDom;
            };
            
            addFilter("init", function(){
                tim.dom();
            });
        }
        
        return tim;
    }());

    return tim;

}, this);

(function(context) {

		/*
		 * Swipe 2.0
		 *
		 * Brad Birdsall
		 * Copyright 2013, MIT License
		 *
		*/

		function Swipe(container, options) {

		  "use strict";

		  // utilities
		  var noop = function() {}; // simple no operation function
		  var offloadFn = function(fn) { setTimeout(fn || noop, 0) }; // offload a functions execution

		  // check browser capabilities
		  var browser = {
			addEventListener: !!window.addEventListener,
			touch: ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
			transitions: (function(temp) {
			  var props = ['transitionProperty', 'WebkitTransition', 'MozTransition', 'OTransition', 'msTransition'];
			  for ( var i in props ) if (temp.style[ props[i] ] !== undefined) return true;
			  return false;
			})(document.createElement('swipe'))
		  };

		  // quit if no root element
		  if (!container) return;
		  var element = container.children[0];
		  var slides, slidePos, width, length;
		  options = options || {};
		  var index = parseInt(options.startSlide, 10) || 0;
		  var speed = options.speed || 300;
		  options.continuous = options.continuous !== undefined ? options.continuous : true;

		  function setup() {

			// cache slides
			slides = element.children;
			length = slides.length;

			// set continuous to false if only one slide
			if (slides.length < 2) options.continuous = false;

			//special case if two slides
			if (browser.transitions && options.continuous && slides.length < 3) {
			  element.appendChild(slides[0].cloneNode(true));
			  element.appendChild(element.children[1].cloneNode(true));
			  slides = element.children;
			}

			// create an array to store current positions of each slide
			slidePos = new Array(slides.length);

			// determine width of each slide
			width = container.getBoundingClientRect().width || container.offsetWidth;

			element.style.width = (slides.length * width) + 'px';

			// stack elements
			var pos = slides.length;
			while(pos--) {

			  var slide = slides[pos];

			  slide.style.width = width + 'px';
			  slide.setAttribute('data-index', pos);

			  if (browser.transitions) {
				slide.style.left = (pos * -width) + 'px';
				move(pos, index > pos ? -width : (index < pos ? width : 0), 0);
			  }

			}

			// reposition elements before and after index
			if (options.continuous && browser.transitions) {
			  move(circle(index-1), -width, 0);
			  move(circle(index+1), width, 0);
			}

			if (!browser.transitions) element.style.left = (index * -width) + 'px';

			container.style.visibility = 'visible';

		  }

		  function prev() {

			if (options.continuous) slide(index-1);
			else if (index) slide(index-1);

		  }

		  function next() {

			if (options.continuous) slide(index+1);
			else if (index < slides.length - 1) slide(index+1);

		  }

		  function circle(index) {

			// a simple positive modulo using slides.length
			return (slides.length + (index % slides.length)) % slides.length;

		  }

		  function slide(to, slideSpeed) {

			// do nothing if already on requested slide
			if (index == to) return;

			if (browser.transitions) {

			  var direction = Math.abs(index-to) / (index-to); // 1: backward, -1: forward

			  // get the actual position of the slide
			  if (options.continuous) {
				var natural_direction = direction;
				direction = -slidePos[circle(to)] / width;

				// if going forward but to < index, use to = slides.length + to
				// if going backward but to > index, use to = -slides.length + to
				if (direction !== natural_direction) to =  -direction * slides.length + to;

			  }

			  var diff = Math.abs(index-to) - 1;

			  // move all the slides between index and to in the right direction
			  while (diff--) move( circle((to > index ? to : index) - diff - 1), width * direction, 0);

			  to = circle(to);

			  move(index, width * direction, slideSpeed || speed);
			  move(to, 0, slideSpeed || speed);

			  if (options.continuous) move(circle(to - direction), -(width * direction), 0); // we need to get the next in place

			} else {

			  to = circle(to);
			  animate(index * -width, to * -width, slideSpeed || speed);
			  //no fallback for a circular continuous if the browser does not accept transitions
			}

			index = to;
			offloadFn(options.callback && options.callback(index, slides[index]));
		  }

		  function move(index, dist, speed) {

			translate(index, dist, speed);
			slidePos[index] = dist;

		  }

		  function translate(index, dist, speed) {

			var slide = slides[index];
			var style = slide && slide.style;

			if (!style) return;

			style.webkitTransitionDuration =
			style.MozTransitionDuration =
			style.msTransitionDuration =
			style.OTransitionDuration =
			style.transitionDuration = speed + 'ms';

			style.webkitTransform = 'translate(' + dist + 'px,0)' + 'translateZ(0)';
			style.msTransform =
			style.MozTransform =
			style.OTransform = 'translateX(' + dist + 'px)';

		  }

		  function animate(from, to, speed) {

			// if not an animation, just reposition
			if (!speed) {

			  element.style.left = to + 'px';
			  return;

			}

			var start = +new Date;

			var timer = setInterval(function() {

			  var timeElap = +new Date - start;

			  if (timeElap > speed) {

				element.style.left = to + 'px';

				if (delay) begin();

				options.transitionEnd && options.transitionEnd.call(event, index, slides[index]);

				clearInterval(timer);
				return;

			  }

			  element.style.left = (( (to - from) * (Math.floor((timeElap / speed) * 100) / 100) ) + from) + 'px';

			}, 4);

		  }

		  // setup auto slideshow
		  var delay = options.auto || 0;
		  var interval;

		  function begin() {

			interval = setTimeout(next, delay);

		  }

		  function stop() {

			delay = 0;
			clearTimeout(interval);

		  }


		  // setup initial vars
		  var start = {};
		  var delta = {};
		  var isScrolling;

		  // setup event capturing
		  var events = {

			handleEvent: function(event) {

			  switch (event.type) {
				case 'touchstart': this.start(event); break;
				case 'touchmove': this.move(event); break;
				case 'touchend': offloadFn(this.end(event)); break;
				case 'webkitTransitionEnd':
				case 'msTransitionEnd':
				case 'oTransitionEnd':
				case 'otransitionend':
				case 'transitionend': offloadFn(this.transitionEnd(event)); break;
				case 'resize': offloadFn(setup); break;
			  }

			  if (options.stopPropagation) event.stopPropagation();

			},
			start: function(event) {

			  var touches = event.touches[0];

			  // measure start values
			  start = {

				// get initial touch coords
				x: touches.pageX,
				y: touches.pageY,

				// store time to determine touch duration
				time: +new Date

			  };

			  // used for testing first move event
			  isScrolling = undefined;

			  // reset delta and end measurements
			  delta = {};

			  // attach touchmove and touchend listeners
			  element.addEventListener('touchmove', this, false);
			  element.addEventListener('touchend', this, false);

			},
			move: function(event) {

			  // ensure swiping with one touch and not pinching
			  if ( event.touches.length > 1 || event.scale && event.scale !== 1) return

			  if (options.disableScroll) event.preventDefault();

			  var touches = event.touches[0];

			  // measure change in x and y
			  delta = {
				x: touches.pageX - start.x,
				y: touches.pageY - start.y
			  }

			  // determine if scrolling test has run - one time test
			  if ( typeof isScrolling == 'undefined') {
				isScrolling = !!( isScrolling || Math.abs(delta.x) < Math.abs(delta.y) );
			  }

			  // if user is not trying to scroll vertically
			  if (!isScrolling) {

				// prevent native scrolling
				event.preventDefault();

				// stop slideshow
				stop();

				// increase resistance if first or last slide
				if (options.continuous) { // we don't add resistance at the end

				  translate(circle(index-1), delta.x + slidePos[circle(index-1)], 0);
				  translate(index, delta.x + slidePos[index], 0);
				  translate(circle(index+1), delta.x + slidePos[circle(index+1)], 0);

				} else {

				  delta.x =
					delta.x /
					  ( (!index && delta.x > 0               // if first slide and sliding left
						|| index == slides.length - 1        // or if last slide and sliding right
						&& delta.x < 0                       // and if sliding at all
					  ) ?
					  ( Math.abs(delta.x) / width + 1 )      // determine resistance level
					  : 1 );                                 // no resistance if false

				  // translate 1:1
				  translate(index-1, delta.x + slidePos[index-1], 0);
				  translate(index, delta.x + slidePos[index], 0);
				  translate(index+1, delta.x + slidePos[index+1], 0);
				}

			  }

			},
			end: function(event) {

			  // measure duration
			  var duration = +new Date - start.time;

			  // determine if slide attempt triggers next/prev slide
			  var isValidSlide =
					Number(duration) < 250               // if slide duration is less than 250ms
					&& Math.abs(delta.x) > 20            // and if slide amt is greater than 20px
					|| Math.abs(delta.x) > width/2;      // or if slide amt is greater than half the width

			  // determine if slide attempt is past start and end
			  var isPastBounds =
					!index && delta.x > 0                            // if first slide and slide amt is greater than 0
					|| index == slides.length - 1 && delta.x < 0;    // or if last slide and slide amt is less than 0

			  if (options.continuous) isPastBounds = false;

			  // determine direction of swipe (true:right, false:left)
			  var direction = delta.x < 0;

			  // if not scrolling vertically
			  if (!isScrolling) {

				if (isValidSlide && !isPastBounds) {

				  if (direction) {

					if (options.continuous) { // we need to get the next in this direction in place

					  move(circle(index-1), -width, 0);
					  move(circle(index+2), width, 0);

					} else {
					  move(index-1, -width, 0);
					}

					move(index, slidePos[index]-width, speed);
					move(circle(index+1), slidePos[circle(index+1)]-width, speed);
					index = circle(index+1);

				  } else {
					if (options.continuous) { // we need to get the next in this direction in place

					  move(circle(index+1), width, 0);
					  move(circle(index-2), -width, 0);

					} else {
					  move(index+1, width, 0);
					}

					move(index, slidePos[index]+width, speed);
					move(circle(index-1), slidePos[circle(index-1)]+width, speed);
					index = circle(index-1);

				  }

				  options.callback && options.callback(index, slides[index]);

				} else {

				  if (options.continuous) {

					move(circle(index-1), -width, speed);
					move(index, 0, speed);
					move(circle(index+1), width, speed);

				  } else {

					move(index-1, -width, speed);
					move(index, 0, speed);
					move(index+1, width, speed);
				  }

				}

			  }

			  // kill touchmove and touchend event listeners until touchstart called again
			  element.removeEventListener('touchmove', events, false)
			  element.removeEventListener('touchend', events, false)

			},
			transitionEnd: function(event) {

			  if (parseInt(event.target.getAttribute('data-index'), 10) == index) {

				if (delay) begin();

				options.transitionEnd && options.transitionEnd.call(event, index, slides[index]);

			  }

			}

		  }

		  // trigger setup
		  setup();

		  // start auto slideshow if applicable
		  if (delay) begin();


		  // add event listeners
		  if (browser.addEventListener) {

			// set touchstart event on element
			if (browser.touch) element.addEventListener('touchstart', events, false);

			if (browser.transitions) {
			  element.addEventListener('webkitTransitionEnd', events, false);
			  element.addEventListener('msTransitionEnd', events, false);
			  element.addEventListener('oTransitionEnd', events, false);
			  element.addEventListener('otransitionend', events, false);
			  element.addEventListener('transitionend', events, false);
			}

			// set resize event on window
			window.addEventListener('resize', events, false);

		  } else {

			window.onresize = function () { setup() }; // to play nice with old IE

		  }

		  // expose the Swipe API
		  return {
			setup: function() {

			  setup();

			},
			slide: function(to, speed) {

			  // cancel slideshow
			  stop();

			  slide(to, speed);

			},
			prev: function() {

			  // cancel slideshow
			  stop();

			  prev();

			},
			next: function() {

			  // cancel slideshow
			  stop();

			  next();

			},
			stop: function() {

			  // cancel slideshow
			  stop();

			},
			getPos: function() {

			  // return current index position
			  return index;

			},
			getNumSlides: function() {

			  // return total number of slides
			  return length;
			},
			kill: function() {

			  // cancel slideshow
			  stop();

			  // reset element
			  element.style.width = '';
			  element.style.left = '';

			  // reset slides
			  var pos = slides.length;
			  while(pos--) {

				var slide = slides[pos];
				slide.style.width = '';
				slide.style.left = '';

				if (browser.transitions) translate(pos, 0, 0);

			  }

			  // removed event listeners
			  if (browser.addEventListener) {

				// remove current event listeners
				element.removeEventListener('touchstart', events, false);
				element.removeEventListener('webkitTransitionEnd', events, false);
				element.removeEventListener('msTransitionEnd', events, false);
				element.removeEventListener('oTransitionEnd', events, false);
				element.removeEventListener('otransitionend', events, false);
				element.removeEventListener('transitionend', events, false);
				window.removeEventListener('resize', events, false);

			  }
			  else {

				window.onresize = null;

			  }

			}
		  }

		}


		if ( window.jQuery || window.Zepto ) {
		  (function($) {
			$.fn.Swipe = function(params) {
			  return this.each(function() {
				$(this).data('Swipe', new Swipe($(this)[0], params));
			  });
			}
		  })( window.jQuery || window.Zepto )
		}
		

	$.extend(context,{
		Swipe: Swipe
	});

}(window[ns]));


/* SimpleResponsiveFader */
(function($,context) {

	$.fn.SimpleResponsiveFader = function(opts) {			
		
		var SRFInterface = {
			allElements 	: this,	//store the jQuery object passed to the plugin in allElements for easy retrieval
			moveTo 		: function(i) { this.allElements.each(function() { var SRF = $(this).data('SimpleResponsiveFader'); if(SRF) { SRF.moveTo(i); } }); return this; },
			pause			: function() { this.allElements.each(function() { var SRF = $(this).data('SimpleResponsiveFader'); if(SRF) { SRF.pause(); } }); return this; },
			start				: function(_speed) { this.allElements.each(function() { var SRF = $(this).data('SimpleResponsiveFader'); if(SRF) { SRF.start(_speed); } }); return this; },
			
			//get the raw JavaScript SRF object
			raw				: function() { return this.allElements.data('SimpleResponsiveFader'); },
			
			
			//return to jQuery chainability
			end				: function() { return this.allElements.each(function() { }); }
		}
		
		this.each(function() { if(!$(this).data('SimpleResponsiveFader')) { $(this).data('SimpleResponsiveFader',new SimpleResponsiveFader($(this),opts)); } });	

		return SRFInterface;
		
	}
	
	
	function SimpleResponsiveFader(el,opts) {
		var o = this;
		
		this.state = {
			holder			:	el,
			list				:	el.children('.slider'),
			items			:	el.children('.slider').children(),
			total				:	el.children('.slider').children().length,
			interval			:	false,
			current			:	0,
			controls		:	false
		}
		
		if(!this.state.items.length) { return; }
		
		this.settings = $.extend({},$.fn.SimpleResponsiveFader.defaults,opts);
		this.state.items.eq(0).siblings().css('opacity',0);
		this.addListeners();
		this.state.holder.trigger('SRFInit',[this]);
		this.start();		
		
	}
	
	SimpleResponsiveFader.prototype.addListeners = function() {
		var 
			self 			=	this,
			holder		=	$('<div class="control-div"/>');
			controls 	= 	$('<ul class="slider_controls"/>'),
			links 		= 	$('<ul class="slider_links"/>'),
			linkCb		=	function(e) { self.moveTo($(this).closest('li').index()); $(this).closest('li').addClass('selected').siblings().removeClass('selected'); },
			controlCb	=	function(e) {
				var 
					el 		= 	$(this),
					which	= 	(el.hasClass('next') ? 1 : -1),
					next		=	(+self.state.current)+which,
					actual	=	(next >= self.state.items.length) ? 0 : next;
					
				self.moveTo(actual);
			};
			
		if(this.settings.pauseOnHover) {

			this.state.holder
				.on('mouseenter',function() { self.pause(); })
				.on('mouseleave',function() { self.start(); });
		}
			
		if(this.settings.buildControls && this.state.total > 1) {
			controls
				.append('<li><a class="next">&raquo;</a></li>')
				.append('<li><a class="prev">&laquo;</a></li>');
			
			holder.append(controls);
			this.state.controls = controls.find('a').on('click',controlCb);
		}
		
		if(this.settings.separateControls && this.settings.separateControls.length) { 
			this.settings.separateControls.on('click',controlCb);
		}
		
		if(this.settings.buildLinks && this.state.total > 1) {
			this.state.items.each(function(index,el) { links.append('<li><a>'+((+index)+1)+'</a></li>'); });
			holder.append(links);
			this.state.links = links.find('a').on('click',linkCb);
		}
		
		this.state.total > 1 && this.state.holder.prepend(holder);
		
		if(this.settings.separateLinks && this.settings.separateLinks.length) { this.settings.separateLinks.on('click',linkCb); }
		if(this.settings.centerLinks && this.state.links && this.state.links.length) { links.css('marginLeft',-(this.state.links.closest('ul').outerWidth()/2)).css('left','50%'); }
		
		//add the highlight link class to the selected link
		if(this.settings.highlightLink && this.state.links && this.state.links.length) {		
			links.find('a').eq(this.state.current).parent().addClass(this.settings.highlightLink);
		}
		
	
	}
	
	SimpleResponsiveFader.prototype.pause		=	function() {
		clearInterval(this.state.interval);
		this.state.paused = true;
		this.state.holder.trigger('SRFPause');
	}
	
	SimpleResponsiveFader.prototype.start		=	function(_speed,notrigger) {
		var 
				self 		= 	this,
				next		=	self.state.current-0+1,
				actual	=	(next >= this.state.items.length) ? 0 : next;
		
		this.state.paused = false;
		this.settings.speed = _speed || this.settings.speed;
		this.state.interval = setTimeout(function() { 
			self.moveTo(actual); 
		},self.settings.delay);
		
		if(!notrigger) { this.state.holder.trigger('SRFPause'); }
	}
	
	SimpleResponsiveFader.prototype.moveTo = function(i) {
		var 
			self		=	this,
			next 		= 	this.state.items.eq(i),
			current 	=	this.state.items.eq(this.state.current),
			finish	=	function() {
				next
					.siblings()
					.css('position','absolute')
					.css('opacity',0)
					.css('zIndex',-1)
					.end()
					.css('position','relative')
					.css('zIndex',1);
				
				self.state.animating = false;
				self.state.holder.trigger('SRFFadeEnd',[self]);
				self.state.paused || self.start(false,true);
			};
		
		if(this.state.animating || !next.length || this.state.current === i) { return; }
		clearInterval(this.state.interval);
		
		this.state.current = next.index();
		this.state.holder.trigger('SRFFadeStart',[this]);
		if(this.settings.highlightLink && this.state.links && this.state.links.length) {		
			this.state.links.eq(this.state.current).parent().addClass(this.settings.highlightLink).siblings().removeClass(this.settings.highlightLink);
		}
		this.state.animating = true;
		next.css('display','block').css('position','absolute').css('opacity',0).css('zIndex',10);
		
		if(this.settings.resizeHeight) { this.state.holder.stop().animate({height:next.outerHeight()},this.settings.resizeHeightSpeed,this.settings.resizeHeightEasing); }
		
		switch(this.settings.fadeType) {
			case 'in-out':
				current.stop().animate({opacity:0},this.settings.speed,this.settings.easing,function() {				
					next.stop().animate({opacity:1},self.settings.speed,self.settings.easing,finish);
				});			
				break;
			default:
				next.stop().animate({opacity:1},this.settings.speed,this.settings.easing,finish,function() {
					current.stop().animate({opacity:0},self.settings.speed,self.settings.easing);
				});
				break;
		}
		
	}
	
	
	$.fn.SimpleResponsiveFader.defaults = {
		delay					:	3000,
		easing					:	'swing',
		speed					:	1000,
		pauseOnHover		:	false,
		fadeType				:	'cross',			//string	- cross | in-out				-  	the fader will cross fade, or fade completely out and then in
		
		//animate the height when switching slides?
		resizeHeight				:	false,
		resizeHeightSpeed		:	1000,
		resizeHeightEasing	:	'swing',
		
		//next/previous controls
		buildControls			:	false,
		separateControls	:	false,
		
		
		//1, 2, 3, 4, ... links
		buildLinks				:	false,
		separateLinks		:	false,
		centerLinks			:	true,
		highlightLink			:	'selected'		//class name to be applied to the selected link
	}

	$.extend(context,{
		SimpleResponsiveFader: SimpleResponsiveFader
	});
	
}(jQuery,window[ns]));


(function(context) {

	var 
		Swipe = context.Swipe,
		SimpleResponsiveFader = context.SimpleResponsiveFader,
		buildSwipe,
		buildSRF;
		
	//SRF
	(function() {
		buildSRF = function() {
			//if a touch enabled device, use a swiper instead
			if(Modernizr.touch && !$(this).hasClass('force-srf')) {
				$(this)
					.removeClass('srf')
					.addClass('swipe')
					.children()
					.removeClass('slider')
					.addClass('swipe-wrap')
				return;
			}
		
				var 
					el = $(this);
					
				if($('html').hasClass('ie8') && el.data('noie8')) { return; }
					
				el
					.children()
					.addClass('slider')
					.end()
					.SimpleResponsiveFader({
						delay			:	( (+el.data('auto')*1000) || 5000),
						speed			:	500,
						resizeHeight	:	false,
						resizeHeightSpeed: 500,
						fadeType		:	el.data('type') || 'cross',
						pauseOnHover : !!(el.data('pauseonhover')),
						buildLinks		: 	!!(el.data('controls')),
						centerLinks	: 	false,
						buildControls	:	!!(el.data('links'))
					});
					
					if(!el.data('auto')) { el.SimpleResponsiveFader().pause(); }
		};
		
		$('div.srf').each(function() { buildSRF.apply(this,arguments); });
	}());
	
	//swiper
	(function() {
		buildSwipe = function() {
			var 
				swipe,
				el = $(this),
				//links = $(this).find('div.links a'),
				children = el.children('div.swipe-wrap').children('div');
				
				if(el.data('swipe')) { return; }
				
				if(children.length == 1) {
					el.css('visibility','visible');
				} else {
				
					swipe = new Swipe(el[0],{
						continuous: (el.data('notcontinuous') !== undefined) ? el.data('notcontinuous') : true,
						auto:( (+el.data('auto')*1000) || false),
						startSlide: (el.data('start')) || 0,
						stopOnClick:  (el.data('stoponclick') !== undefined) ? el.data('stoponclick') : true,
						speed: (+el.data('speed')) || 500,
						callback: function(pos,div) {
							var 
								i,
								d = $(div).data();
								
								if(d) {
									i = (typeof d.originalindex === 'undefined') ? d.index : d.originalindex;
								} else {
									i = pos;
									//return;
								}
							
							$('div.swipe',el).each(function() { buildSwipe.apply(this,arguments); });
						
							el.trigger('swipeChanged',[i,div]);
							if(el.data('controls') || el.data('controlbar')) {	
								el.parent().find('div.slider-controls a,div.slider-controls-links a.item').eq(i).addClass('selected').siblings().removeClass('selected');
							}
							
						}
					});
				
				}
				
				if(el.closest('div.swiper-wrapper').hasClass('paginated-swiper')) {
					(function() {
					
						if(children.length <= 1) {
							el.closest('div.swiper-wrapper').addClass('hide-links');
							return;
						}
					
						var
							pel = el.closest('div.swiper-wrapper'),
							paginationEl = $('div.pagination',pel),
							total = $('span.total',paginationEl),
							current = $('span.current',paginationEl),
							swiperEl = pel.children('div.swipe'),
							swiper = swiperEl.data('swipe'),
							perSlide = $('div.grid',pel).eq(0).children().length,
							totalCount = parseInt(total.html(),10);
							
							paginationEl.removeClass('hide');
							
							swiperEl.on('swipeChanged',function(e,i,div) {
								current.html('Page '+(i+1)+' of '+children.length);
							});
							
							
					}());
				}
				
				el.children('div.swipe-wrap').children('div').each(function(i,el) {
					if(i > children.length-1) {
						$(this).data('originalindex',((i%children.length))).addClass('ghost');
					}
				});
				
				el.data('swipe',swipe);
				
			if(children.length > 1) {
			
				if(el.data('controlbar')) {
				
					var controlString = '<div class="slider-controls-links swiper-nav-bar pagination"><div><a class="previous sprite" title="Previous">Previous</a>';
					children.each(function(i,el) {
						controlString += '<a class="item '+((!i) ? 'selected' : '')+'">'+(i+1)+'</a>';
					});
					controlString += '<a class="next sprite" title="Next">Next</a></div></div>';
					el
						.parent()
						.append(controlString)
						.on('click','div.slider-controls-links a',function(e) {
							e.preventDefault();
							var el = $(this);
							
							e.preventDefault();
							
							if(el.hasClass('next')) {
								swipe.next();
								return;
							}
							
							if(el.hasClass('prev')) {
								swipe.prev();
								return;
							}
							
							swipe.slide(el.parent().children('a.item').index(el));
						});
						
					return;
				
				}

			
				if(el.data('controls')) {
					
					var controlString = '<div class="slider-controls"><div>';
					children.each(function(i,el) {
						controlString += '<a class="'+((!i) ? 'selected' : '')+'">'+(i+1)+'</a>';
					});
					controlString += '</div></div>';
					el
						.parent()
						.append(controlString);
					
					
				}
			
				if(el.data('links')) {
					el
						.parent()
						.append('<div class="links"><a class="previous sprite" title="Previous">Previous</a><a class="next sprite" title="Next">Next</a></div>');
				}
				
				//add the listeners
					el
						.parent()
						.on('click','div.slider-controls a',function(e) {
							e.preventDefault();
							swipe.slide($(this).index());
							
							//restart, if required
							
							
						})
						.on('click','div.links a',function(e) {
							
							e.preventDefault();
							var el = $(this);
							if(el.hasClass('next')) {
								swipe.next();
							} else {
								swipe.prev();
							}
						});
			}
		};

		$('div.swipe').each(function() { buildSwipe.apply(this,arguments); });
	}());


	$.extend(context,{
		SwipeSRF: {
			buildSwipe: buildSwipe,
			buildSRF: buildSRF					
		}
	});

}(window[ns]));

(function(context) {

	if(window.devicePixelRatio) {
	
		if(window.devicePixelRatio > 2 ) { //i.e. 2.5
			$('html').addClass('threex');
			
		} else if(window.devicePixelRatio > 1) { //i.e. 1.5
			$('html').addClass('twox');
		}
	}

}(window[ns]));

(function(context) {

	$(window)
		.on('load resize',function() {
			var blocks = $('.blocks').filter(function() { return !$(this).hasClass('always-list'); });
			
			if(window.outerWidth < 650) {
				blocks.addClass('list-view');
			} else {
				blocks.removeClass('list-view');
			}
		}).trigger('resize');
		
	var checkBlockControls = function() {
		var el = $(this);
		
		el.find('div.swipe').each(function() {
			var swiper = $(this).data('swipe');
			
			if(swiper && swiper.getNumSlides() > 1) {
				el.addClass('with-swiper');
				
				el.on('click','button.control',function() {
					var pos = $(this).hasClass('next') ? 1 : -1;
					swiper.slide(swiper.getPos()+pos);
				});
				
			} else {
				el.removeClass('with-swiper');
			}
		});
	};

	$('.blocks')
			.each(function() {
				checkBlockControls.apply(this);
			})
			.on('gridReloaded',function() {
				checkBlockControls.apply(this);
			});
		
}(window[ns]));

(function(context) {
	
	var 
		$locations = $('div.locations'),
		$locationsButtons = $locations.find('div.buttons button'),
		$locationsSwiperEl = $locations.find('div.swipe'),
		locationsSwiper = $locationsSwiperEl.data('swipe');
		
		$locationsButtons
			.eq(0)
			.addClass('selected')
			.end()
			.on('click',function(e) {
				locationsSwiper.slide($(this).index());
			});
		
		$locationsSwiperEl.on('swipeChanged',function(e,i,div) {
			$locationsButtons.removeClass('selected').eq(i).addClass('selected');
		});
	
}(window[ns]));

(function(SwipeSRF) {


	$('div.hero').each(function() {
		var
			el = $(this),
			swiperEl = $('div.swipe',el),
			swiper = swiperEl.data('swipe'),
			allSlides = swiperEl.find('div.swipe-wrap').children(),
			realSlides = allSlides.filter(function() { return !$(this).hasClass('ghost'); }),
			fakeSlides = allSlides.filter(function() { return realSlides.index(this) === -1 });
			utils = {
			
				loadBackgroundImageForSlideAtIndex: function(i) {
					
					var 
						slide = realSlides.eq(i),
						source = slide.data('src');
						
					if(slide.hasClass('loaded')) { return; }
					
					$('<img/>')
						.on('load',function() {
							realSlides
								.eq(i)
								.add(fakeSlides.eq(i))
								.children('div.item')
								.css('backgroundImage','url('+source+')')
								.parent()
								.addClass('loaded');
							
							//first load
							if(i === 0) {
								el.addClass('loaded');
							}
							
						})
						.attr('src',source);
					
				}
			
			};
			
			swiperEl.on('swipeChanged',function(e,i) {
				utils.loadBackgroundImageForSlideAtIndex(i);
			});
			
			//load the first background image immediately
			utils.loadBackgroundImageForSlideAtIndex(0);
			
	});

}(window[ns].SwipeSRF));

(function(SwipeSRF,raf) {


	var 
		$document = $(document),
		$body = $('body');

	//mobile nav
	(function() {
	
		var cb = function(e) { 
			var el = e.target ? e.target : e.srcElement;
			
			if($(el).hasClass('page-wrapper')) {
				$body.removeClass('animating');//.scrollTop(0); 
			}
		}
		$document.on('click','#mobile-nav',function(e) {
			e.preventDefault();
			$body.toggleClass('show-nav').addClass('animating');
			return false;
		});
		
		$body
			.on({
				'webkitTransitionEnd': cb,
				'msTransitionEnd': cb,
				'oTransitionEnd': cb,
				'otransitionend': cb,
				'transitionend': cb
			});
		
	}());


	var 
		nav = $('nav'),
		searchArea = $('div.nav div.search-area'),
		bookAppointment = nav.find('div.nav-book-appointment');

	//change to fixed position
	var fixedPositionNav = {
		
		originalPosition: nav.offset().top,
		isProcessing: false,
		lastScrollY: $(window).scrollTop(),
		
		requestProcess: function() {
			if(!this.isProcessing) {
				this.isProcessing = true;
				this.lastScrollY = $(window).scrollTop();
				raf.requestAnimFrame(this.process,this);
			}
		},
		
		process: function() {
			var self = fixedPositionNav;
			if(self.lastScrollY >= self.originalPosition) {
				nav.addClass('fixed');
			} else {
				nav.removeClass('fixed');
			}
			
			self.isProcessing = false;
		}
		
	};
	
	$(window).on('scroll',function() { fixedPositionNav.requestProcess(); });
	//check upon page load the position of the nav
	fixedPositionNav.requestProcess();
	
	//manage book appointment
	bookAppointment.each(function() {
		var el = $(this);
		el.children('button').on('click',function(e) {
			el.toggleClass('expanded');
			
			if(el.hasClass('expanded')) {
				el.find('input').eq(0).focus();
			} else {
				el.find('input,textarea').blur();
			}
			
		});
		
		
		$document
			.on('click',function(e) {
			
				//close the book appointment widget when clicked outside
				if(bookAppointment.hasClass('expanded') && !bookAppointment.find(e.target).length) {
					bookAppointment.removeClass('expanded');
					return;
				}
				
				//close the search box when clicked outside
				if(searchArea.hasClass('expanded') && !searchArea.find(e.target).length) {
					searchArea.removeClass('expanded');
					return;
				}
			});
		
	});
	
	//sliders in nav
	nav
		.on('mouseenter','div.sw>ul>li',function(e) {
			var
				el  =$(this),
				dd = el.children('div'),
				swiperEl = dd.find('div.swipe-to-be'),
				swiper = swiperEl.data('swipe');
				
				if(!swiperEl.hasClass('swipe')) {
					//build for the first time
					SwipeSRF.buildSwipe.apply(swiperEl.addClass('swipe'));
				} else {
					//rebuild
					swiper.setup();
				}
				
				swiperEl.trigger('swipeChanged',[0,swiperEl.find('div.img').eq(0).parent()[0]]);
				
		})
		.on('mouseenter','li li a',function(e) {
			var
				el = $(this),
				index = el.parent().index(),
				dd = el.closest('div'),
				swiper = dd.find('div.swipe');
				
				swiper.data('swipe').slide(index);
		})
		.on('swipeChanged',function(e,index,element) {
			var
				source,
				el = $(element),
				imgEl = el.find('div.img'),
				imgSrc = imgEl.data('img');
				
				if(imgEl.data('working')) { return; }
				
				imgEl.data('working',true);
				
				if(imgSrc) {
					source = templateJS.templateURL+'/'+imgSrc;
					$('<img/>')
						.on('load',function() {
							imgEl
								.css('backgroundImage','url('+source+')')
								.addClass('loaded')
								.removeData('working')
								.removeData('img');
						})
						.on('error',function() {
							imgEl.removeData('working');
						})
						.attr('src',source);
				}
				
		});
		
	//toggle search box
	searchArea.each(function() {
		var input = searchArea.find('input');
		
		searchArea.children('button').on('click',function(e) {
			searchArea.toggleClass('expanded');
			if(searchArea.hasClass('expanded')) {
				input.focus();
			} else {
				input.blur();
			}
		});
	});
		

	//no public API
	return {};

}(window[ns].SwipeSRF,window[ns].raf));

(function(ScrollTo,SwipeSRF) {
		
	var 
		loadingProcess = false, //jqXHR
		help = $('section.how-can-we-help'),
		answersContainer = help.find('.how-can-we-help-results'),
		answersError = answersContainer.find('div.error'),
		servicesContainer = help.find('.how-can-we-help-services'),
		servicesButtons = help.find('button.filter'),
		servicesSwiper = servicesContainer.find('div.services-swiper'),
		services = servicesContainer.find('div.services-source .service'),
		servicesCount = servicesContainer.find('div.count span.num'),
		tabs = help.find('div.tab'),
		tabSwiperEl = help.find('div.tab-swiper'),
		tabSwiper = tabSwiperEl.data('swipe'),
		methods = {
		
			cancelLoading: function() {
				loadingProcess && loadingProcess.abort();
			},
		
			toggleExpand: function() {
				var isExpanded = help.hasClass('expanded');
				if(isExpanded) {
					help.removeClass('expanded');
				} else {
					help.addClass('expanded');
					tabSwiper.setup();
				}
			},
			
			doneLoadingQuestion: function(response) {
				answersContainer.find('div.answers').html(response);
			},
			
			failedLoadingQuestion: function() {
				//show a failed loading message
				answersError.removeClass('i-hidden');
			},
			
			loadQuestion: function(url,q) {			
				var self = this;
				this.showLoading(true);
				
				answersError.addClass('i-hidden');
				answersContainer.find('div.answers').html('');
				
				
				loadingProcess = $.ajax({
					type: 'get',
					url:url,
					dataType: 'html',
					data: {
						q:q
					}
				})
					.done(function(r,status,jqXHR) {
						if(status === 'success') {
							return self.doneLoadingQuestion(r)
						}
						
						self.failedLoadingQuestion(jqXHR);
					})
					.fail(function(r,status,jqXHR) {
						if(status !== 'abort') {
							self.failedLoadingQuestion();
						}
					})
					.always(function() {
						self.showLoading(false);
					});
				
			},
			
			showLoading: function(show) {
			
			},
			
			filterServices: function() {
				var 
					filteredServiceDivs,
					numFound = 0,
					servicesPerSlide = 5,
					contentHTML = '',
					slideHTML = '',
					filters = [],
					swipeTemplate = '\
						<div class="swiper-wrapper collapse-750">\
							<div class="swipe">\
								<div class="swipe-wrap">\
									{{contents}}\
								</div><!-- .swipe-wrap -->\
							</div><!-- .swipe -->\
						</div><!-- .swiper-wrapper -->',
					contentTemplate = '\
						<div>\
							<div class="grid-wrap">\
								<div class="grid eqh fill collapse-no-flex centered">\
									{{contents}}\
								</div><!-- .grid -->\
							</div><!-- .grid-wrap -->\
						</div>';
						

				servicesSwiper.html('');
				
				//set up filters
				servicesButtons
					.filter(function() { return $(this).hasClass('active') })
					.filter(function() { filters.push($(this).data('slug')); });
					
				//loop through services divs
				filteredServiceDivs = services.filter(function() {
					var
						found = false,
						el = $(this),
						services = el.data('services');
						
						$.each(services,function(i,service) {
							if(filters.indexOf(service) !== -1) {
								found = true;
								numFound++;
								return false;
							}
						});
						
						return found;
				});
				
				for(var i=0;i<Math.ceil(filteredServiceDivs.length/servicesPerSlide);i++) {
					var start = (i*servicesPerSlide);
					slideHTML = '';
					filteredServiceDivs
						.slice(start,start+servicesPerSlide)
						.each(function(i,el) {
							slideHTML += el.outerHTML;
						});
						
					contentHTML += tim(contentTemplate,{contents:slideHTML});
					
				}
				
				servicesCount.html(numFound);
				SwipeSRF.buildSwipe.apply(servicesSwiper.html(tim(swipeTemplate,{contents:contentHTML})).find('div.swipe'));
				servicesSwiper.trigger('gridReloaded');
				
			}
			
		};
		
		
		//toggle the help section
		help
			.on('click','button.toggle',function(e) {
				methods.toggleExpand();
			})
			.on('submit','form',function(e) {
				e.preventDefault();
				var input = $(this).find('input');
				methods.loadQuestion(this.action,input.val());
			});
		
		//filter
		servicesButtons.on('click',function(e) {
			var 
				el = $(this),
				buttonEl = el.children('span.sprite-after'),
				sprite = el.data('sprite'),
				isActive = el.hasClass('active'),
				newClass = sprite + (isActive ? '-g' : ''),
				currentClass = sprite + (isActive ? '' : '-g');
				
				buttonEl.removeClass(currentClass).addClass(newClass);
				
				if(isActive) {
					el.removeClass('active');
				} else {
					el.addClass('active');
				}
				
				methods.filterServices();
				
		});
		
		//highlight the first tab always
		tabs.eq(0).addClass('active');
		
		//show the questions/services divs when the slider has changed
		tabSwiperEl.on('swipeChanged',function(e,i,div) { 
			methods.cancelLoading();
			
			tabs.removeClass('active').eq(i).addClass('active'); 
			
			servicesContainer.addClass('hidden');
			answersContainer.addClass('hidden');
			
			switch(i) {
				case 2: //services
					servicesContainer.removeClass('hidden');
					methods.filterServices();
					break;
				case 1: //question
					answersContainer.removeClass('hidden');
					break;
			}
			
		});
		
		//click listener for the tabs
		tabs.on('click',function(e) {
		
			ScrollTo('div.how-can-we-help-body',{offsetTop:50});
		
			var index = $(this).index();
			if(tabSwiper.getPos() !== index) {
				tabSwiper.slide(index);
			}
		});
	


}(window[ns].ScrollTo,window[ns].SwipeSRF));

//ios - good to use on combination with touch
Modernizr.addTest('ios',function() {
	return window.navigator.userAgent.match(/ios/i);
});

